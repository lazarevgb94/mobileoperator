var contextPath = window.location.pathname.substring(0, window.location.pathname.indexOf("/"));



$(document).ready(function () {
    var table = $('#promoTable').DataTable({
        "sAjaxSource": contextPath + "/tariffs",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "aoColumns": [
            {"mData": "id"},
            {"mData": "name"},
            {"mData": "sms"},
            {"mData": "minutes"},
            {"mData": "internet"},
            {"mData": "description"}
        ]
    })
})