import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ApplicationScoped
@ServerEndpoint("/socket")
public class EndpointController {

    @Inject
    private EndpointModel endpointModel;

    @OnOpen
    public void open(Session session) {
        endpointModel.addUser(session);
    }

    @OnClose
    public void close(Session session) {
        endpointModel.removeUser(session);
    }
}
