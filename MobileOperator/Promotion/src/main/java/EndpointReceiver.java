import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/*import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

*//*@RabbitListener(bindings = @QueueBinding(value = @Queue(),
        exchange = @Exchange(value = , type = ExchangeTypes.FANOUT)))*//*
public class EndpointReceiver implements MessageListener {

    @Inject
    private EndpointModel model;

    @Override
    public void onMessage(Message message) {
        model.sendMessageAll();
    }
}*/
@RabbitListener(queues = "myQueue")
public class EndpointReceiver{
    @RabbitHandler
    public void receive(String message){
        System.out.println("Received: " + message);
    }
}
