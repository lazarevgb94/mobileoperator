import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


@ApplicationScoped
public class EndpointModel {
    private final Logger logger = LoggerFactory.getLogger(EndpointModel.class);

    private Set<Session> users = new HashSet<>();

    public void sendMessageAll() {
        for (Session user : users) {
            try {
                user.getBasicRemote().sendText("UPDATE");
            } catch (IOException ioe) {
                logger.error(ioe.getMessage());
                removeUser(user);
            }
        }
    }

    public void addUser(Session session) {
        users.add(session);
    }

    public void removeUser(Session session) {
        users.remove(session);
    }
}
