<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create an account</title>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<div class="">
    <form:form modelAttribute="account" method="post" action="${contextPath}/admin/addAccount">

        Login: <form:input path="login" class="input100" type="text"
                    placeholder="Your login"/>
        <span class="focus-input100"></span>

        Password: <form:password path="password" class="input100"
                                 pattern="[a-zA-z0-9]{8,}"/>
        <span class="focus-input100"></span>

        <input type="submit" value="Add account"/>
    </form:form>

    <form method="get" action="${contextPath}/admin/returnToAdminPage">
        <input type="submit" value="Return"/>
    </form>
</div>
</body>
</html>
