<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="/struts-tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<html>
<head>
    <title>Client page</title>
    <jsp:include page="header.jsp"/>
    <jsp:include page="footer.jsp"/>
    <c:set value="${pageContext.request.contextPath}" var="contextPath"/>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #17a2b8;
        }

        li {
            float: left;
            margin-left: 10px;
            margin-top: 12px;
        }

        button {
            cursor: pointer;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }
    </style>

</head>
<body>
<%--<c:set var="lock" value="${contract.block}"/>--%>
<div>
    <%-- <c:if test="${lock == 'CLIENT_BLOCKED'}">
         <c:set value="Your number is blocked" var="phrase"/>
         <ul>
             <li>
                 <form action="${contextPath}/client/blockSelf" method="post">
                     <button type="submit" id="btn-block" class="btn-lg">Unblock number</button>
                 </form>
             </li>
             <li>
                 <form action="${contextPath}/client/showPersonalContract" method="get">
                     <button type="submit" class="btn-lg">Contract</button>
                 </form>
             </li>
             <li>
                 <form action="${contextPath}/client/showTariffs" method="get">
                     <button type="submit" class="btn-lg" disabled>Tariffs</button>
                 </form>
             </li>
             <li>
                 <form action="${contextPath}/client/editClientOptions" method="get">
                     <button type="submit" class="btn-lg" disabled>Options</button>
                 </form>
             </li>
             <li>
                 <form action="${contextPath}/login" method="get">
                     <button type="submit" class="btn-lg">Quit</button>
                 </form>
             </li>
         </ul>
     </c:if>--%>

    <%--   <c:if test="${lock == 'MANAGER_BLOCKED'}">
           <c:set value="Your number has been blocked by employee of Glebafon :(" var="phrase"/>
           <ul>
               <li>
                   <form action="${contextPath}/client/showPersonalContract" method="get">
                       <button type="submit" class="btn-lg">Contract</button>
                   </form>
               </li>
               <li>
                   <form action="${contextPath}/client/showTariffs" method="get">
                       <button type="submit" class="btn-lg" disabled>Tariffs</button>
                   </form>
               </li>
               <li>
                   <form action="${contextPath}/client/editClientOptions" method="get">
                       <button type="submit" class="btn-lg" disabled>Options</button>
                   </form>
               </li>
               <li>
                   <form action="${contextPath}/login" method="get">
                       <button type="submit" class="btn-lg">Quit</button>
                   </form>
               </li>
           </ul>
       </c:if>--%>

    <c:set value="Your number has been blocked by employee of Glebafon :(" var="phrase"/>
    <ul>
        <li>
            <form:form id="blockForm" action="${contextPath}/client/blockSelf" method="post">
                <button type="submit" id="btn-block" class="btn-lg">Block number</button>
            </form:form>
        </li>
        <%--<li>
              <form action="${contextPath}/client/showPersonalContract" method="get">
                  <button type="submit" id="btn-contract" class="btn-lg">Contract</button>
              </form>
          </li>--%>
        <li>
            <form action="${contextPath}/client/balance" method="get">
                <button type="submit" id="btn-balance" class="btn-lg">Balance</button>
            </form>
        </li>
        <li>
            <form action="${contextPath}/client/showTariffs" method="get">
                <button type="submit" id="btn-tariffs" class="btn-lg">Tariffs</button>
            </form>
        </li>
        <li>
            <form action="${contextPath}/client/editClientOptions" method="get">
                <button type="submit" id="btn-options" class="btn-lg">Options</button>
            </form>
        </li>
        <li>
            <form action="${contextPath}/login" method="get">
                <button type="submit" id="btn-quit" class="btn-lg">Quit</button>
            </form>
        </li>
    </ul>
</div>


<div class="display-4">
    <div class="row" style="background: #fcf8e3">
        <label class="col-sm-2">Client:</label>
        <div class="col-sm-10" id="client">
            <p>${contract.client.name} ${contract.client.surname}</p>
        </div>
    </div>

    <div class="row" style="background: #9fcdff">
        <label class="col-sm-2">Number:</label>
        <div class="col-sm-10">${contract.number}</div>
    </div>

    <div class="row" style="background: #fcf8e3">
        <label class="col-sm-2">Tariff:</label>
        <div class="col-sm-10">${contract.tariff.name}</div>
    </div>

    <div class="row" style="background: #9fcdff">
        <label class="col-sm-2">Options:</label>
        <div class="col-sm-10">
            <c:if test="${empty contract.additions}">
                <c:out value="No options"/>
            </c:if>
            <c:forEach var="addition" items="${contract.additions}" varStatus="loop">
                <c:out value="${addition.name}"/>
                <c:if test="${not loop.last}">,</c:if>
            </c:forEach>
        </div>
    </div>

    <div class="row" style="background: #fcf8e3">
        <label class="col-sm-2">Balance:</label>
        <div class="col-sm-10">${contract.balance} &#8381</div>
    </div>
</div>
<script src="<c:url value="/resources/scripts/simpleAjax.js"/> "></script>
</body>
</html>
