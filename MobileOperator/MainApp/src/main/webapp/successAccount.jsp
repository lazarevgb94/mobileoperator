<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Success</title>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
    <div class="">
        <h1>New account was created successfully!</h1>
    </div>
    <form action="${contextPath}/returnToAdminPage" method="get">
        <input type="submit" value="Return">
    </form>
</body>
</html>
