<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
    <title>Tariffs</title>
    <jsp:include page="header.jsp"/>
</head>
<body style="margin-left: 100px; margin-top: 20px">
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<form:form method="get" action="${contextPath}/client/returnToClientPage">
    <button type="submit" class="btn btn-primary btn-lg">Return</button>
</form:form>
<div style="float: left; margin-right: 100px">
    <h1>All Tariffs</h1>
    <table class="table table-striped" style="font-size: x-large">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>SMS</th>
            <th>Minutes</th>
            <th>Internet</th>
            <th>Price</th>
        </tr>
        </thead>

        <c:forEach var="tariff" items="${tariffList}">
            <tr>
                <td>${tariff.id}</td>
                <td>${tariff.name}</td>
                <td>${tariff.sms}</td>
                <td>${tariff.minutes}</td>
                <td>${tariff.internet} Gb</td>
                <td>${tariff.price} &#8381</td>
                <td>
                    <c:choose>
                        <c:when test="${tariff.id eq contract.tariff.id}">
                            <c:out value="This tariff is yours"/>
                        </c:when>
                        <c:otherwise>
                            <form action="${contextPath}/client/${tariff.id}/chooseTariff" method="post">
                                <input type="submit" value="Add to basket"/>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div style="float: left; font-size: x-large">
    <div class="text-lg-center">
        <h1>Current balance: ${contract.balance} &#8381</h1>
    </div>
    <div>
        <h1 class="text-center" id="btnBasket">Basket</h1>
    </div>
    <div id="divBasket" style="display: none">
        <table class="table table-bordered table-striped text-center" style="font-size: x-large">
            <c:if test="${not basket.free}">
                <tr class="thead-dark">
                    <th style="background: #FFFFFF; border: 0"></th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Act-cost</th>
                </tr>
                <c:if test="${basket.chosenTariff != null}">
                    <tr>
                        <td>
                            <form action="${contextPath}/client/removeBasketTariffFromOptions" method="post">
                                <input type="submit" value="X">
                            </form>
                        </td>
                        <td>
                            <p><b>Tariff: </b>${basket.chosenTariff.name}</p>
                        </td>
                        <td>${basket.chosenTariff.price}</td>
                        <td>0</td>
                    </tr>
                </c:if>

                <c:if test="${not empty basket.chosenAdditions}">
                    <c:forEach varStatus="loop" items="${basket.chosenAdditions}" var="addition">
                        <tr>
                            <td>
                                <form action="${contextPath}/client/${addition.id}/removeBasketAddFromOptions"
                                      method="post">
                                    <input type="submit" value="X">
                                </form>
                            </td>
                            <td>${addition.name}</td>
                            <td>${addition.price}</td>
                            <td>${addition.additionActivationCost}</td>
                        </tr>
                    </c:forEach>
                </c:if>
            </c:if>
        </table>

        <c:if test="${not empty basket.deletedAdditions}">
            <b>Remove options:</b><br/>
            <c:forEach varStatus="loop" items="${basket.deletedAdditions}" var="addition">
                <form action="${contextPath}/client/${addition.id}/removeBasketAddFromOptions" method="post">
                    <input type="submit" value="X"><c:out value="${addition.name}"/>
                </form>
            </c:forEach>
        </c:if>

        <br/><br/><b>Price:</b>
        <c:out value="${basket.totalPrice}" default="0.0"/> &#8381 <br/>
        <b>Total activation cost:</b>
        <c:out value="${basket.totalActivationCost}" default="0.0"/> &#8381 <br/><br/>
        <b>TOTAL:</b>
        <c:out value="${basket.totalSum}" default="0.0"/> &#8381 <br/>

        <form action="${contextPath}/client/submitBasket" method="post">
            <c:choose>
                <c:when test="${basket.free and empty basket.deletedAdditions}">
                    <input type="submit" value="Buy it!" disabled/>
                </c:when>
                <c:otherwise>
                    <c:if test="${contract.balance < basket.totalSum}">
                        <p style="color: #b21f2d">Please top up your balance!</p>
                        <input type="submit" value="Buy it!" disabled/>
                    </c:if>
                    <c:if test="${contract.balance >= basket.totalSum}">
                        <input type="submit" value="Buy it!"/>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"/>
<script>
    $(function () {
        $("#btnBasket").click(function () {
            $("#divBasket").slideToggle("fast");
        })
    })
</script>
</body>
</html>
