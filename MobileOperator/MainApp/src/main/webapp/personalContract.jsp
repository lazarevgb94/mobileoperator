<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Personal contracts list</title>
    <jsp:include page="header.jsp"/>
</head>
<body class="display-4">
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<form:form method="get" modelAttribute="contract" action="${contextPath}/client">
    <div class="row" style="background: #fcf8e3">
        <label class="col-sm-2">Client:</label>
        <div class="col-sm-10">${contract.client.name} ${contract.client.surname}</div>
    </div>

    <div class="row" style="background: #9fcdff">
        <label class="col-sm-2">Number:</label>
        <div class="col-sm-10">${contract.number}</div>
    </div>

    <div class="row" style="background: #9fcdff">
        <label class="col-sm-2">Balance:</label>
        <div class="col-sm-10">${contract.balance}</div>
    </div>

    <div class="row" style="background: #fcf8e3">
        <label class="col-sm-2">Tariff:</label>
        <div class="col-sm-10">${contract.tariff.name}</div>
    </div>

    <div class="row" style="background: #9fcdff">
        <label class="col-sm-2">Options:</label>
        <div class="col-sm-10">
            <c:if test="${empty contract.additions}">
                <c:out value="No options"/>
            </c:if>
            <c:forEach var="addition" items="${contract.additions}" varStatus="loop">
                <c:out value="${addition.name}"/>
                <c:if test="${not loop.last}">,</c:if>
            </c:forEach>
        </div>
    </div>
    <input type="submit" value="Return" class="btn btn-lg btn-secondary" style="width: 200px">
</form:form>
<jsp:include page="footer.jsp"/>
</body>
</html>
