<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: gleb
  Date: 17.09.18
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts</title>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<form:form method="get" action="${contextPath}/returnToAdminPage">
    <table>
        <tr>
            <th>Id</th>
            <th>Login</th>
            <th>Password</th>
        </tr>
        <c:forEach items="${accounts}" var="acc">
            <tr>
                <td>${acc.id}</td>
                <td>${acc.login}</td>
                <td>${acc.password}</td>
            </tr>
        </c:forEach>
    </table>
    <input type="submit" value="Return">
</form:form>
</body>
</html>
