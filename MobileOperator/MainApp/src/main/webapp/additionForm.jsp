<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="header.jsp"/>
    <title>Addition</title>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<div class="container">

    <c:choose>
        <c:when test="${addition['new']}">
            <h1>Add addition</h1>
        </c:when>
        <c:otherwise>
            <h1>Update addition</h1>
        </c:otherwise>
    </c:choose>
    <br/>

    <spring:url value="/employee/additions" var="additionActionUrl"/>

    <form:form class="form-horizontal" method="post" modelAttribute="addition" action="${additionActionUrl}">

        <form:hidden path="id"/>

        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <form:input path="name" type="text" class="form-control " id="name" placeholder="Name"/>
                    <form:errors path="name" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="parameter">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Parameter</label>
                <div class="col-sm-10">
                    <form:select path="parameter">
                        <c:forEach items="${parameterEnum}" var="parameter">
                            <form:option value="${parameter}"/>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="value">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Value</label>
                <div class="col-sm-10">
                    <form:input path="value" type="text" class="form-control" id="value" placeholder="value"/>
                    <form:errors path="value" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="price">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Price</label>
                <div class="col-sm-10">
                    <form:input path="price" type="text" class="form-control" id="price" placeholder="price"/>
                    <form:errors path="price" class="control-label"/>
                </div>
            </div>
        </spring:bind>

        <spring:bind path="additionActivationCost">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Activation cost</label>
                <div class="col-sm-10">
                    <form:input path="additionActivationCost" type="text" class="form-control"
                                id="additionActivationCost" placeholder="Actisvation cost"/>
                    <form:errors path="additionActivationCost" class="control-label"/>
                </div>
            </div>
        </spring:bind>
        <spring:bind path="tariffIds">
            <div class="form-group">
                <label class="col-sm-2 control-label">Incompatible tariffs</label>
                <div class="col-sm-10">
                    <form:select path="tariffIds" multiple="true" size="">
                        <c:forEach varStatus="loop" items="${tariffList}" var="tariff">
                            <form:option value="${tariff.id}">${tariff.name} (${tariff.description})</form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </spring:bind>
        <div class="form-group">
            <label class="col-sm-2 control-label">Mandatory options</label>
            <div class="col-sm-10">
                <form:select path="mandatoryIds" multiple="true" size="">
                    <c:forEach varStatus="loop" items="${additionList}" var="add">
                        <form:option value="${add.id}">${add.name}</form:option>
                    </c:forEach>
                </form:select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Incompatible options</label>
            <div class="col-sm-10">
                <form:select path="incompatibleIds" multiple="true" size="">
                    <c:forEach varStatus="loop" items="${additionList}" var="add">
                        <form:option value="${add.id}">${add.name}</form:option>
                    </c:forEach>
                </form:select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <c:choose>
                    <c:when test="${addition['new']}">
                        <button type="submit" class="btn-lg btn-primary pull-right">Add</button>
                    </c:when>
                    <c:otherwise>
                        <button type="submit" class="btn-lg btn-primary pull-right">Update</button>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </form:form>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>