<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>New client</title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<h2>Submitted client info</h2>
<form:form method="get" action="${contextPath}/employee">
    Name: ${name}<br/>
    Surname: ${surname}<br/>
    Birthday: ${birthday}<br/>
    Passport<br/>
    Series: ${series}<br/>
    Number: ${number}<br/>
    Issued by: ${issuedBy}<br/>
    Issued date: ${issueDate}<br/>
    Division code: ${divisionCode}<br/>
    Address: ${address}<br/>
    Email: ${email}<br/>
    Contract number: ${contractNumber}<br/>

    <input type="submit" value="Return">
</form:form>
<jsp:include page="footer.jsp"/>
</body>
</html>
