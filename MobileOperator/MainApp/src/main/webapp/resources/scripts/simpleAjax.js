$(document).ready(function () {
    $("#btn-block").click(function () {
        $.ajax({
            type: "POST",
            url: $("#blockForm").attr("action"),

            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application-json");
            },

            success: function (data) {
                $('#client').html(data);
                /*var lock = contract.block;
                disableButtons(lock);*/
            }
        });
    })
});

function disableButtons(lock) {
    if (lock == "CLIENT_BLOCKED") {
        $("#btn-block").prop("textContent", "Unblock number");
        $("#btn-tariffs").prop("disabled", true);
        $("#btn-options").prop("disabled", true);
    }
    else if (lock == "UNBLOCKED") {
        $("#btn-block").prop("textContent", "Block number");
        $("#btn-tariffs").prop("disabled", false);
        $("#btn-options").prop("disabled", false);
    }
    /* var status = $("#btn-tariffs").prop("disabled");
     if (status == true) {
         $("#btn-block").prop("textContent", "Block number");
         $("#btn-tariffs").prop("disabled", false);
         $("#btn-options").prop("disabled", false);
     }
     else {
         $("#btn-block").prop("textContent", "Unblock number");
         $("#btn-tariffs").prop("disabled", true);
         $("#btn-options").prop("disabled", true);
     }*/
}
