<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
    <h2>Hello, admin!</h2>
    <form action="${contextPath}/admin/showAccounts" method="get">
        <input type="submit" value="Show registered managers"/>
    </form>

    <form action="${contextPath}/admin/addAccountForm" method="get">
        <input type="submit" value="Register new manager"/>
    </form>

    <button type="button" name="back" onclick="history.back()">back</button>
</body>
</html>
