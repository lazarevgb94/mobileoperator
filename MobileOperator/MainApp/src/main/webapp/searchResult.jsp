<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<c:set value="${pageContext.request.contextPath}" var="contextPath"/>
<form method="get" action="${contextPath}/employee">
    <h4 class="text-center">По данному номеру найден следующий пользователь</h4><br/>
    Name: ${clientDto.name}<br/>
    Surname: ${clientDto.surname}<br/>
    Birthday: ${clientDto.birthday}<br/>
    Passport<br/>
    Series: ${clientDto.passportSeries}<br/>
    Number: ${clientDto.passportNumber}<br/>
    Issued by: ${clientDto.passportIssuedBy}<br/>
    Issued date: ${clientDto.passportIssueDate}<br/>
    Division code: ${clientDto.passportDivisionCode}<br/>
    Address: ${clientDto.address}<br/>
    Email: ${clientDto.email}<br/>
    <input type="submit" value="Return">
</form>
<jsp:include page="footer.jsp"/>
</body>
</html>
