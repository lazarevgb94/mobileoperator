package validators;

import model.dto.BigClientDto;
import model.services.ClientService;
import model.services.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component
public class ClientValidator implements Validator {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ContractService contractService;

    @Override
    public boolean supports(Class<?> clazz) {
        return BigClientDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BigClientDto bcd = (BigClientDto) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required.name", "Field name is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthday", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passportSeries", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passportNumber", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passportIssuedBy", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passportIssueDate", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passportDivisionCode", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tariffId", "Required");

        Pattern patternNumber = Pattern.compile("\\+7-[0-9]{3}-[0-9]{3}-[0-9]{4}");
        if(!patternNumber.matcher(bcd.getNumber()).matches())
            errors.rejectValue("number", "Pattern.contract.number");

        if (contractService.getByNumber(bcd.getNumber())!=null)
            errors.rejectValue("number", "Unique.contract.number");
    }
}
