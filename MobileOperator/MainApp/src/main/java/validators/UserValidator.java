package validators;

import model.dto.ClientDto;
import model.dto.ContractDto;
import model.dto.UserDto;
import model.services.ClientService;
import model.services.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
  private static final String PASSWORD = "password";

    @Autowired
    private ContractService contractService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDto user = (UserDto) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "Required");
        //7: a@bk.ru
        if (user.getNumber().length() <11) {
            errors.rejectValue("number", "Size.user.number");
        }

        ContractDto contractDto = contractService.getByNumber(user.getNumber());
        if (contractDto == null) {
            errors.rejectValue("contract", "No.user.contract");
        }
        else {
            if (contractDto.getPassword() != null) {
                errors.rejectValue(PASSWORD, "Different.user.alreadyHas");
            }

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, PASSWORD, "Required");
            if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
                errors.rejectValue(PASSWORD, "Size.user.password");
            }

            if (!user.getConfirmPassword().equals(user.getPassword())) {
                errors.rejectValue("confirmPassword", "Different.userForm.password");
            }
        }
    }
}
