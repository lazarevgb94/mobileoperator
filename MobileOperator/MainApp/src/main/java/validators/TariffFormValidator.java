package validators;

import model.dto.TariffFormDto;
import model.services.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Component
public class TariffFormValidator implements Validator {
    @Autowired
    TariffService tariffService;

    @Override
    public boolean supports(Class<?> clazz) {
        return TariffFormDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TariffFormDto tariff = (TariffFormDto) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
                "NotEmpty.tariffForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description",
                "NotEmpty.tariffForm.description");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price",
                "NotEmpty.tariffForm.price");
        if (tariff.getPrice().compareTo(BigDecimal.ZERO) < 0)
            errors.rejectValue("price", "Diff.tariffForm.price");
    }
}
