package model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import model.entities.Addition;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * DTO for Option
 * It contains: option's name, its price and a cost of option's service activation
 */
public class AdditionDto extends AbstractDto implements Serializable {
    private String name;
    private Addition.Parameter parameter;
    private int value;
    private BigDecimal price;
    private BigDecimal additionActivationCost;
    @JsonBackReference
    private List<PackageDto> packages = new ArrayList<>();
    private Set<AdditionDto> mandatoryAdditions = new HashSet<>();
    private Set<AdditionDto> incompatibleAdditions = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAdditionActivationCost() {
        return additionActivationCost;
    }

    public void setAdditionActivationCost(BigDecimal additionActivationCost) {
        this.additionActivationCost = additionActivationCost;
    }

    public Addition.Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Addition.Parameter parameter) {
        this.parameter = parameter;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public List<PackageDto> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageDto> packages) {
        this.packages = packages;
    }

    public Set<AdditionDto> getMandatoryAdditions() {
        return mandatoryAdditions;
    }

    public void setMandatoryAdditions(Set<AdditionDto> mandatoryAdditions) {
        this.mandatoryAdditions = mandatoryAdditions;
    }

    public Set<AdditionDto> getIncompatibleAdditions() {
        return incompatibleAdditions;
    }

    public void setIncompatibleAdditions(Set<AdditionDto> incompatibleAdditions) {
        this.incompatibleAdditions = incompatibleAdditions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdditionDto that = (AdditionDto) o;
        return value == that.value &&
                Objects.equals(name, that.name) &&
                parameter == that.parameter &&
                Objects.equals(price, that.price) &&
                Objects.equals(additionActivationCost, that.additionActivationCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parameter, value, price, additionActivationCost);
    }


}
