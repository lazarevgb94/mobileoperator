package model.dto;

import model.entities.AdditionRelation.Relation;

import java.io.Serializable;
import java.util.Objects;

public class AdditionRelationDto implements Serializable {
    private AdditionDto mainAddition;
    private AdditionDto relativeAddition;
    private Relation relation;

    public AdditionDto getMainAddition() {
        return mainAddition;
    }

    public void setMainAddition(AdditionDto mainAddition) {
        this.mainAddition = mainAddition;
    }

    public AdditionDto getRelativeAddition() {
        return relativeAddition;
    }

    public void setRelativeAddition(AdditionDto relativeAddition) {
        this.relativeAddition = relativeAddition;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdditionRelationDto that = (AdditionRelationDto) o;
        return Objects.equals(mainAddition, that.mainAddition) &&
                Objects.equals(relativeAddition, that.relativeAddition) &&
                relation == that.relation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mainAddition, relativeAddition, relation);
    }
}
