package model.dto;

import java.io.Serializable;
import java.util.Objects;

public class PackageDto implements Serializable {
    private TariffDto tariff;
    private AdditionDto addition;

    public TariffDto getTariff() {
        return tariff;
    }

    public void setTariff(TariffDto tariff) {
        this.tariff = tariff;
    }

    public AdditionDto getAddition() {
        return addition;
    }

    public void setAddition(AdditionDto addition) {
        this.addition = addition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageDto that = (PackageDto) o;
        return Objects.equals(tariff, that.tariff) &&
                Objects.equals(addition, that.addition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tariff, addition);
    }
}
