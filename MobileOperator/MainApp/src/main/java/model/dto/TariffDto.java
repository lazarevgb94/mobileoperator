package model.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * DTO for Tariff.
 * It contains: tariff's name, its price and set of available options.
 */

public class TariffDto extends AbstractDto implements Serializable {
    private String name;
    private int sms;
    private int minutes;
    private int internet;
    private String description;
    private BigDecimal price;
    /*@JsonManagedReference*/
    private List<PackageDto> packages = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getSms() {
        return sms;
    }

    public void setSms(int sms) {
        this.sms = sms;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getInternet() {
        return internet;
    }

    public void setInternet(int internet) {
        this.internet = internet;
    }

    public List<PackageDto> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageDto> packages) {
        this.packages = packages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TariffDto tariffDto = (TariffDto) o;
        return sms == tariffDto.sms &&
                minutes == tariffDto.minutes &&
                internet == tariffDto.internet &&
                Objects.equals(name, tariffDto.name) &&
                Objects.equals(description, tariffDto.description) &&
                Objects.equals(price, tariffDto.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sms, minutes, internet, description, price);
    }
/* public List<AdditionDto> getIncompatibleAdditions() {
        return incompatibleAdditions == null ? new ArrayList<>() : incompatibleAdditions;
    }

    public void setIncompatibleAdditions(List<AdditionDto> incompatibleAdditions) {
        this.incompatibleAdditions = incompatibleAdditions;
    }


    public void addIncompatibleAddition(AdditionDto additionDto) {
        this.getIncompatibleAdditions().add(additionDto);
        if (!additionDto.getIncompatibleTariffs().contains(this)) {
            additionDto.addIncompatibleTariff(this);
        }
    }*/
}
