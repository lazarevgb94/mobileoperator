package model.services;

import model.dto.AccountDto;
import model.dto.ContractDto;
import model.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountService extends model.services.Service {
    void create(AccountDto account);

    List<AccountDto> getAll();

    void update(UserDto user);

    ContractDto getContractByNumber(String number);
}
