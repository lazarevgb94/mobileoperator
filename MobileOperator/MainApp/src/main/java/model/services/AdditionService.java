package model.services;

import model.dto.AdditionDto;
import model.dto.AdditionFormDto;
import model.dto.TariffDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdditionService extends model.services.Service {
    void create(AdditionDto option);

    List<AdditionDto> getAll();

    /**
     * Retrieves a list of options that have the specified identifier
     *
     * @param additionIds specified identifiers
     * @return list of AdditionDto
     */
    List<AdditionDto> getListById(List<Integer> additionIds);

    void deleteById(int id);

    void saveOrUpdate(AdditionFormDto addition);

   AdditionDto getById(Integer id);

    List<TariffDto> getAllTariffs();

    void updateTariff(TariffDto tariffDto);
}
