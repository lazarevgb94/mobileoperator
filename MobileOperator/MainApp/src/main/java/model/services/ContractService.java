package model.services;

import model.dto.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ContractService extends model.services.Service {
    void create(ContractDto contract);

    List<ContractDto> getAll();

    ContractDto getByNumber(String number);

    /**
     * Retrieves contract according to its identifier
     *
     * @param id identifier of contract
     * @return instance of ContractDto
     */
    ContractDto getById(int id);

    /**
     * Retrieves a list of all existed tariffs
     *
     * @return list of TariffDto
     */
    List<TariffDto> getTariffList();

    /**
     * Retrieves tariff according to its identifier
     *
     * @param id identifier of tariff
     * @return tariff
     */
    TariffDto getTariffById(int id);

    /**
     * Updates specified contract
     *
     * @param contract new contract
     */
    void updateContract(ContractDto contract);

    /**
     * Inverses status of the supplied contract by manager.
     * If it's blocked, changes status to unblocked.
     * If it's unblocked, changes status ti blocked by manager.
     *
     * @param contract supplied contract
     */
    void inverseBlockStatus(ContractDto contract);

    /**
     * Inverses status of the supplied contract by client.
     * If it's blocked by client, changes status to unblocked.
     * If it's unblocked, changes status ti blocked by client.
     *
     * @param contract supplied contract
     */
    void inverseClientBlockStatus(ContractDto contract);

    /**
     * Retrieves list of identifiers of all available options for this contract
     *
     * @param contractDto specified contract
     * @return list of identifiers
     */
    List<Integer> getAvailableOptionsIds(ContractDto contractDto);

    /**
     * Retrieves list of all available options for this contract
     *
     * @param contractDto specified contract
     * @return list of available additions
     */
    List<AdditionDto> getAvailableOptions(ContractDto contractDto);

    /**
     * Retrieves addition with specified identifier
     *
     * @param aId identifier
     * @return addition
     */
    AdditionDto getAdditionById(int aId);

    /**
     * Retrieves list of options according to list of identifiers
     *
     * @param optionIds identifiers
     * @return list of chosen options
     */
    List<AdditionDto> getAdditionListByIdList(List<Integer> optionIds);

    /**
     * Transfers data from client's basket to DB and then clears the basket
     *
     * @param contractDto contract of client
     * @param basket      client's basket
     */
    void submitBasket(ContractDto contractDto, Basket basket);

    ClientDto getClientById(int id);

    List<AdditionDto> getAllAdditions();

    List<ClientDto> getAllClients();

    void deleteById(int id);

    ContractDto getByPassword(String password);

    void addClientContract(int id, ContractFormDto contractFormDto);
}