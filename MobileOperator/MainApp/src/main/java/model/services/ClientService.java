package model.services;

import model.dto.*;
import model.entities.Client;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClientService extends model.services.Service {
    ClientDto getById(Integer id);

    void create(ClientDto client);

    List<ClientDto> getAll();

    void deleteById(Integer id);

    void update(ClientDto dto);

    ClientDto getByPassword(String password);

    ContractDto getContractById(int id);

    /**
     * Retrieves a list of all existed tariffs
     * @return list of TariffDto
     */
    List<TariffDto> getTariffList();

    /**
     * Retrieves a list of all existed options
     * @return list of AdditionDto
     */
    List<AdditionDto> getAdditionList();

    /**
     * Build ClientDto instance from BigClient that contains a lot of waste data
     * @param bigClient instance of BigClient that contains client, contract number,
     *                  tariff and options info
     */
    void createFromBigClient(BigClientDto bigClient);
}
