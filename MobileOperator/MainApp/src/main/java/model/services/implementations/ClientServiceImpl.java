package model.services.implementations;

import model.dto.*;
import model.entities.Addition;
import model.entities.Client;
import model.entities.Contract;
import model.entities.Tariff;
import model.repositories.AdditionRepository;
import model.repositories.ClientRepository;
import model.repositories.ContractRepository;
import model.repositories.TariffRepository;
import model.services.ClientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utils.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClientServiceImpl implements ClientService {
    private ClientRepository clientRepository;
    private ContractRepository contractRepository;
    private TariffRepository tariffRepository;
    private AdditionRepository additionRepository;
    private ModelMapper modelMapper;
    private Converter converter;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, ContractRepository contractRepository,
                             TariffRepository tariffRepository, AdditionRepository additionRepository,
                             ModelMapper modelMapper, Converter converter) {
        this.clientRepository = clientRepository;
        this.contractRepository = contractRepository;
        this.tariffRepository = tariffRepository;
        this.additionRepository = additionRepository;
        this.modelMapper = modelMapper;
        this.converter = converter;
    }

    @Override
    @Transactional(readOnly = true)
    public ClientDto getById(Integer id) {
        Client client = clientRepository.getByProperty("id", id);
        return client == null ? null : modelMapper.map(client, ClientDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(ClientDto clientDto) {
        clientDto.setValid(true);
        Client client = modelMapper.map(clientDto, Client.class);
        clientRepository.create(client);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, readOnly = true)
    public List<ClientDto> getAll() {
        List<Client> clients = clientRepository.getAllValid();
        for(Client client : clients){
            List<Contract> contracts = new ArrayList<>();
            client.getContracts().forEach(
                    contract -> {
                        if(contract.getValid()){
                            contracts.add(contract);
                        }
                    }
            );
            client.setContracts(contracts);
        }

        return clients.stream()
                .map(client -> modelMapper.map(client, ClientDto.class))
                .collect(Collectors.toList());

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(Integer id) {
        ClientDto clientDto = getById(id);
        List<ContractDto> contractDtos = clientDto.getContracts();
        for (ContractDto cnt : contractDtos) {
            Contract contract = modelMapper.map(cnt, Contract.class);
            contract.setValid(false);
            contractRepository.update(contract);
        }
        Client client = modelMapper.map(clientDto, Client.class);
        client.setValid(false);
        clientRepository.update(client);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(ClientDto dto) {
        clientRepository.update(modelMapper.map(dto, Client.class));
    }

    @Override
    @Transactional(readOnly = true)
    public ClientDto getByPassword(String password) {
        Client client = clientRepository.getByProperty("password", password);
        return client == null ? null : modelMapper.map(client, ClientDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto getContractById(int id) {
        Contract contract = contractRepository.getByProperty("id", id);
        return contract == null ? null : modelMapper.map(contract, ContractDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TariffDto> getTariffList() {
        List<Tariff> tariffs = tariffRepository.getAllValid();
        return tariffs.isEmpty() ? null :
                tariffs.stream()
                        .map(t -> modelMapper.map(t, TariffDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAdditionList() {
        List<Addition> additions = additionRepository.getAllValid();
        return additions.isEmpty() ? null :
                additions.stream()
                        .map(ad -> modelMapper.map(ad, AdditionDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createFromBigClient(BigClientDto bigClient) {
        ClientDto clientDto = converter.convertToClientDto(bigClient);
        ContractDto contractDto = converter.convertToContractDto(bigClient, clientDto);
        clientDto.getContracts().add(contractDto);
        Client client = modelMapper.map(clientDto, Client.class);
        clientRepository.create(client);
    }
}
