package model.services.implementations;

import model.dto.AccountDto;
import model.dto.ContractDto;
import model.dto.UserDto;
import model.entities.Account;
import model.entities.Client;
import model.entities.Contract;
import model.repositories.AccountRepository;
import model.repositories.ClientRepository;
import model.repositories.ContractRepository;
import model.services.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utils.AttributeName;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AccountServiceImpl implements AccountService, UserDetailsService {
    private AccountRepository accountRepository;
    private ContractRepository contractRepository;
    private ClientRepository clientRepository;
    private ModelMapper modelMapper;

    public AccountServiceImpl(){}

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, ContractRepository contractRepository,
                              ClientRepository clientRepository, ModelMapper modelMapper){
        this.accountRepository = accountRepository;
        this.contractRepository = contractRepository;
        this.clientRepository = clientRepository;
        this.modelMapper = modelMapper;
    }

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(AccountDto account) {
        accountRepository.create(modelMapper.map(account, Account.class));
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccountDto> getAll() {
        List<Account> accounts = accountRepository.getAllValid();
        return accounts.isEmpty() ? null :
                accounts.stream()
                        .map(acc -> modelMapper.map(acc, AccountDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(UserDto user) {
        Contract contract = contractRepository.getByProperty(AttributeName.NUMBER, user.getNumber());
        contract.setPassword(user.getConfirmPassword());
        contractRepository.update(contract);
    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto getContractByNumber(String number) {
        Contract contract = contractRepository.getByProperty(AttributeName.NUMBER, number);
        return contract == null ? null :
                modelMapper.map(contract, ContractDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String number) {
        String username;
        String password;
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        switch (number) {
            case AttributeName.ADMIN:
                username = "admin";
                password = bCryptPasswordEncoder.encode("admin");
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
            case AttributeName.MANAGER:
                username = "manager";
                password = bCryptPasswordEncoder.encode("manager");
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_MANAGER"));
                break;
            default:
                Contract contract = contractRepository.getByProperty("number", number);
                if(contract==null) return null;
                username = contract.getNumber();
                password = bCryptPasswordEncoder.encode(contract.getPassword());
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                break;
        }
        return new User(username, password, grantedAuthorities);
    }
}
