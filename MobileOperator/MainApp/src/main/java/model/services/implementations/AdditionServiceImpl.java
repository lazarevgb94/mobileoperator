package model.services.implementations;

import model.dto.AdditionDto;
import model.dto.AdditionFormDto;
import model.dto.TariffDto;
import model.entities.Addition;
import model.entities.Package;
import model.entities.Tariff;
import model.repositories.AdditionRepository;
import model.repositories.TariffRepository;
import model.services.AdditionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utils.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AdditionServiceImpl implements AdditionService {
    private AdditionRepository additionRepository;
    private ModelMapper modelMapper;
    private TariffRepository tariffRepository;
    private Converter converter;

    @Autowired
    public AdditionServiceImpl(AdditionRepository additionRepository, ModelMapper modelMapper,
                               TariffRepository tariffRepository, Converter converter){
        this.additionRepository = additionRepository;
        this.modelMapper = modelMapper;
        this.tariffRepository = tariffRepository;
        this.converter= converter;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(AdditionDto option) {
        option.setValid(true);
        additionRepository.create(modelMapper.map(option, Addition.class));
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAll() {
        List<Addition> options = additionRepository.getAllValid();
        for(Addition addition : options){
            Set<Package> packages = new HashSet<>();
            addition.getPackages().forEach(
                    pack ->{
                        if(pack.getTariff().getValid()){
                            packages.add(pack);
                        }
                    }
            );
            addition.setPackages(packages);
        }
        return options.stream().map(converter::convertToAdditionDtoFromAddition)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getListById(List<Integer> additionIds) {
        List<AdditionDto> additions = new ArrayList<>();
        for (Integer id : additionIds)
            additions.add(modelMapper.map(additionRepository.getByProperty("id", id), AdditionDto.class));
        return additions;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(int id) {
        Addition addition = additionRepository.getByProperty("id", id);
        addition.setValid(false);
        additionRepository.update(addition);
    }

    @Override
    @Transactional(readOnly = true)
    public AdditionDto getById(Integer id) {
        Addition addition = additionRepository.getByProperty("id", id);
        return addition == null ? null : modelMapper.map(addition, AdditionDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TariffDto> getAllTariffs() {
        List<Tariff> tariffs = tariffRepository.getAllValid();
        return tariffs.isEmpty()?  null :
                tariffs.stream().map(tariff -> modelMapper.map(tariff, TariffDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTariff(TariffDto tariffDto) {
        tariffRepository.update(modelMapper.map(tariffDto, Tariff.class));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(AdditionFormDto additionForm) {
        AdditionDto addition = converter.convertToAdditionDto(additionForm);
        if (getById(addition.getId()) == null) {
            /*additionRepository.create(converter.convertToAdditionFromAdditionDto(addition));*/
            additionRepository.create(modelMapper.map(addition, Addition.class));
        } else {
            /*additionRepository.update(converter.convertToAdditionFromAdditionDto(addition));*/
            additionRepository.update(modelMapper.map(addition, Addition.class));
        }
    }
}
