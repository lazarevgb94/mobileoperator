package model.services.implementations;

import model.dto.AdditionDto;
import model.dto.TariffDto;
import model.dto.TariffFormDto;
import model.entities.Addition;
import model.entities.Contract;
import model.entities.Package;
import model.entities.Tariff;
import model.repositories.AdditionRepository;
import model.repositories.TariffRepository;
import model.services.TariffService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utils.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TariffServiceImpl implements TariffService {
    private TariffRepository tariffRepository;
    private AdditionRepository additionRepository;
    private ModelMapper modelMapper;
    private Converter converter;

    @Autowired
    public TariffServiceImpl(TariffRepository tariffRepository, AdditionRepository additionRepository,
                             ModelMapper modelMapper, Converter converter) {
        this.tariffRepository = tariffRepository;
        this.additionRepository = additionRepository;
        this.modelMapper = modelMapper;
        this.converter = converter;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(TariffDto tariff) {
        tariff.setValid(true);
        tariffRepository.create(modelMapper.map(tariff, Tariff.class));
    }

    @Override
    @Transactional(readOnly = true)
    public List<TariffDto> getAll() {
        List<Tariff> tariffs = tariffRepository.getAllValid();
        for(Tariff tariff : tariffs){
            Set<Package> packages = new HashSet<>();
            tariff.getPackages().forEach(
                    pack -> {
                        if(pack.getAddition().getValid()){
                            packages.add(pack);
                        }
                    }
            );
            tariff.setPackages(packages);
        }
        return tariffs.stream().map(tariff -> modelMapper.map(tariff, TariffDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public TariffDto getById(Integer id) {
        Tariff tariff = tariffRepository.getByProperty("id", id);
        return tariff == null ? null : modelMapper.map(tariff, TariffDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(TariffFormDto tariffForm) {
        TariffDto tariff = converter.convertToTariffDto(tariffForm);
        if (getById(tariff.getId()) == null) {
            tariff.setValid(true);
            tariffRepository.create(modelMapper.map(tariff, Tariff.class));
        } else {
            tariffRepository.update(modelMapper.map(tariff, Tariff.class));
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(int id) {
        Tariff tariff = tariffRepository.getByProperty("id", id);
        tariff.setValid(false);
        tariffRepository.update(tariff);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAllAdditions() {
        List<Addition> additions = additionRepository.getAllValid();
        return additions.isEmpty() ? null :
                additions.stream()
                        .map(add -> modelMapper.map(add, AdditionDto.class))
                        .collect(Collectors.toList());
    }
}
