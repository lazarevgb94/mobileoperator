package model.services.implementations;

import model.dto.*;
import model.entities.Addition;
import model.entities.Addition.Parameter;
import model.entities.Client;
import model.entities.Contract;
import model.entities.Tariff;
import model.repositories.AdditionRepository;
import model.repositories.ClientRepository;
import model.repositories.ContractRepository;
import model.repositories.TariffRepository;
import model.services.ContractService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import utils.AttributeName;
import utils.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContractServiceImpl implements ContractService {
    private ClientRepository clientRepository;
    private ContractRepository contractRepository;
    private TariffRepository tariffRepository;
    private AdditionRepository additionRepository;
    private ModelMapper modelMapper;
    private Converter converter;

    @Autowired
    public ContractServiceImpl(ClientRepository clientRepository, ContractRepository contractRepository,
                               TariffRepository tariffRepository, AdditionRepository additionRepository,
                               ModelMapper modelMapper, Converter converter) {
        this.clientRepository = clientRepository;
        this.contractRepository = contractRepository;
        this.tariffRepository = tariffRepository;
        this.additionRepository = additionRepository;
        this.modelMapper = modelMapper;
        this.converter = converter;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(ContractDto contractDto) {
        Contract contract = new Contract();
        contract.setNumber(contractDto.getNumber());
        contract.setClient(modelMapper.map(contractDto.getClient(), Client.class));
        contract.setTariff(modelMapper.map(contractDto.getTariff(), Tariff.class));
        List<Addition> additions = contractDto.getAdditions().stream()
                .map(add -> modelMapper.map(add, Addition.class))
                .collect(Collectors.toList());
        contract.setAdditions(additions);
        contract.setValid(true);
        contractRepository.create(contract);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ContractDto> getAll() {
        List<Contract> contracts = contractRepository.getAllValid();
        return contracts.isEmpty() ? null :
                contracts.stream()
                        .map(cnt -> modelMapper.map(cnt, ContractDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto getByNumber(String number) {
        Contract contract = contractRepository.getByProperty(AttributeName.NUMBER, number);
        return contract == null ? null : modelMapper.map(contract, ContractDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto getById(int id) {
        Contract contract = contractRepository.getByProperty("id", id);
        return contract == null ? null : modelMapper.map(contract, ContractDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TariffDto> getTariffList() {
        List<Tariff> tariffs = tariffRepository.getAllValid();
        return tariffs.isEmpty() ? null :
                tariffs.stream()
                        .map(t -> modelMapper.map(t, TariffDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public TariffDto getTariffById(int id) {
        Tariff tariff = tariffRepository.getByProperty("id", id);
        return tariff == null ? null : modelMapper.map(tariff, TariffDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateContract(ContractDto contractDto) {
        contractDto.setAdditions(converter.checkOwnAdditions(contractDto, contractDto.getAdditions()));
        Contract contract = modelMapper.map(contractDto, Contract.class);
        contractRepository.update(contract);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void inverseBlockStatus(ContractDto contractDto) {
        if (contractDto.getBlock() == Contract.BLOCK.UNBLOCKED) {
            contractDto.setBlock(Contract.BLOCK.MANAGER_BLOCKED);
        } else {
            contractDto.setBlock(Contract.BLOCK.UNBLOCKED);
        }
        Contract contract = modelMapper.map(contractDto, Contract.class);
        contractRepository.update(contract);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void inverseClientBlockStatus(ContractDto contractDto) {
        if (contractDto.getBlock() == Contract.BLOCK.UNBLOCKED) {
            contractDto.setBlock(Contract.BLOCK.CLIENT_BLOCKED);
        } else if (contractDto.getBlock() == Contract.BLOCK.CLIENT_BLOCKED) {
            contractDto.setBlock(Contract.BLOCK.UNBLOCKED);
        }
        Contract contract = modelMapper.map(contractDto, Contract.class);
        contractRepository.update(contract);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Integer> getAvailableOptionsIds(ContractDto contractDto) {
        TariffDto tariffDto = contractDto.getTariff();
        if (tariffDto == null)
            return new ArrayList<>();
        List<AdditionDto> contractAdditions = contractDto.getAdditions();
        List<Addition> additions = additionRepository.getAllValid();
        List<Integer> ids = new ArrayList<>();
        for (Addition ad : additions) {
            Addition.Parameter parameter = ad.getParameter();
            if (parameter == Parameter.SMS && tariffDto.getSms() == 0 ||
                    parameter == Parameter.MINUTES && tariffDto.getMinutes() == 0 ||
                    parameter == Parameter.INTERNET && tariffDto.getInternet() == 0 ||
                    contractAdditions.contains(modelMapper.map(ad, AdditionDto.class))) {
                break;
            } else {
                ids.add(ad.getId());
            }
        }
        return ids;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAvailableOptions(ContractDto contractDto) {
        List<Addition> additions = additionRepository.getAllValid();
        return converter.checkAdditions(contractDto, additions);
    }

    @Override
    @Transactional(readOnly = true)
    public AdditionDto getAdditionById(int aId) {
        Addition addition = additionRepository.getByProperty("id", aId);
        return addition == null ? null :
                modelMapper.map(addition, AdditionDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAdditionListByIdList(List<Integer> optionIds) {
        List<Addition> additions = new ArrayList<>();
        for (Integer id : optionIds) {
            additions.add(additionRepository.getByProperty("id", id));
        }
        return additions.isEmpty() ? null :
                additions.stream()
                        .map(addition -> modelMapper.map(addition, AdditionDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void submitBasket(ContractDto contractDto, Basket basket) {
        TariffDto tariffDto = basket.getChosenTariff();
        if (tariffDto != null) {
            contractDto.setTariff(tariffDto);
            List<AdditionDto> additions = contractDto.getAdditions();
            if (!additions.isEmpty()) {
                List<AdditionDto> result = converter.checkOwnAdditions(contractDto, additions);
                contractDto.setAdditions(result);
            }
        }

        List<AdditionDto> deletedAdditions = new ArrayList<>(basket.getDeletedAdditions());
        if (!deletedAdditions.isEmpty())
            contractDto.getAdditions().removeAll(deletedAdditions);

        List<AdditionDto> chosenAdditions = new ArrayList<>(basket.getChosenAdditions());
        if (!chosenAdditions.isEmpty()) {
            List<Addition> additions = chosenAdditions.stream()
                    .map(add -> modelMapper.map(add, Addition.class))
                    .collect(Collectors.toList());
            List<AdditionDto> result = converter.checkAdditions(contractDto, additions);
            if (result != null) {
                contractDto.getAdditions().addAll(result);
            }
        }

        contractDto.setBalance(contractDto.getBalance().subtract(basket.getTotalSum()));
        contractRepository.update(modelMapper.map(contractDto, Contract.class));

        basket.clear();
    }

    @Override
    @Transactional(readOnly = true)
    public ClientDto getClientById(int id) {
        Client client = clientRepository.getByProperty("id", id);
        return client == null ? null : modelMapper.map(client, ClientDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getAllAdditions() {
        List<Addition> additions = additionRepository.getAllValid();
        return additions.isEmpty() ? null :
                additions.stream().map(addition -> modelMapper.map(addition, AdditionDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClientDto> getAllClients() {
        List<Client> clients = clientRepository.getAllValid();
        return clients.isEmpty() ? null :
                clients.stream().map(client -> modelMapper.map(client, ClientDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(int id) {
        Contract contract = contractRepository.getByProperty("id", id);
        contract.setValid(false);
        contractRepository.update(contract);
    }

    @Override
    @Transactional(readOnly = true)
    public ContractDto getByPassword(String password) {
        Contract contract = contractRepository.getByProperty("password", password);
        return contract == null ? null :
                modelMapper.map(contract, ContractDto.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addClientContract(int id, ContractFormDto contractFormDto) {
        Client client = clientRepository.getByProperty("id", id);
        ContractDto contractDto = converter.convertToContractDtoFromForm(contractFormDto, client);
        contractRepository.create(modelMapper.map(contractDto, Contract.class));
    }
}