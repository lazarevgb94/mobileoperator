package model.services;

import model.dto.AdditionDto;
import model.dto.TariffDto;
import model.dto.TariffFormDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TariffService extends model.services.Service {
    void create(TariffDto tariff);

    List<TariffDto> getAll();

    TariffDto getById(Integer id);

    void saveOrUpdate(TariffFormDto tariff);

    void deleteById(int id);

    List<AdditionDto> getAllAdditions();
}
