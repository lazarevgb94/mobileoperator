package model.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * Presents table Addition in DB
 */
@Entity
@Table(name = "addition")
public class Addition extends AbstractEntity implements Serializable {

    public enum Parameter {SMS, MINUTES, INTERNET}

    @Column(name = "NAME")
    private String name;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "ACTIVATION_COST")
    private BigDecimal additionActivationCost;

    @Column(name = "PARAMETER")
    @Enumerated(EnumType.STRING)
    private Parameter parameter = Parameter.SMS;

    @Column(name = "VALUE")
    private int value;

    @OneToMany(mappedBy = "addition", cascade = CascadeType.ALL)
    private Set<Package> packages = new HashSet<>();

    @ManyToMany(mappedBy = "additions")
    private List<Contract> contracts;

    @OneToMany(mappedBy = "mainAddition", cascade = CascadeType.ALL)
    private Set<AdditionRelation> mainAdditions = new HashSet<>();

    @OneToMany(mappedBy = "relativeAddition", cascade = CascadeType.ALL)
    private Set<AdditionRelation> relativeAdditions = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAdditionActivationCost() {
        return additionActivationCost;
    }

    public void setAdditionActivationCost(BigDecimal additionActivationCost) {
        this.additionActivationCost = additionActivationCost;
    }

    public List<Contract> getContracts() {
        return contracts==null ? new ArrayList<>() : contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Set<Package> getPackages() {
        return packages;
    }

    public void setPackages(Set<Package> packages) {
        this.packages = packages;
    }


    public Set<AdditionRelation> getMainAdditions() {
        return mainAdditions;
    }

    public void setMainAdditions(Set<AdditionRelation> mainAdditions) {
        this.mainAdditions = mainAdditions;
    }

    public Set<AdditionRelation> getRelativeAdditions() {
        return relativeAdditions;
    }

    public void setRelativeAdditions(Set<AdditionRelation> relativeAdditions) {
        this.relativeAdditions = relativeAdditions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Addition addition = (Addition) o;
        return value == addition.value &&
                Objects.equals(name, addition.name) &&
                Objects.equals(price, addition.price) &&
                Objects.equals(additionActivationCost, addition.additionActivationCost) &&
                parameter == addition.parameter &&
                Objects.equals(packages, addition.packages) &&
                Objects.equals(contracts, addition.contracts) &&
                Objects.equals(mainAdditions, addition.mainAdditions) &&
                Objects.equals(relativeAdditions, addition.relativeAdditions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, additionActivationCost, parameter, value, packages, contracts, mainAdditions, relativeAdditions);
    }

    /*public List<Tariff> getIncompatibleTariffs() {
        return incompatibleTariffs==null ? new ArrayList<>() : incompatibleTariffs;
    }

    public void setIncompatibleTariffs(List<Tariff> incompatibleTariffs) {
        this.incompatibleTariffs = incompatibleTariffs;
    }*/

    /*void addIncompatibleTariff(Tariff tariff){
        this.getIncompatibleTariffs().add(tariff);
        if(!tariff.getIncompatibleAdditions().contains(this)){
            tariff.addIncompatibleAddition(this);
        }
    }*/
}
