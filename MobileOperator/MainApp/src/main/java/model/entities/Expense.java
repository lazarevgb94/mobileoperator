package model.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "expense")
public class Expense extends AbstractEntity implements Serializable {

    @Column(name = "EXPENSE_CONTRACT_ID", nullable = false)
    private Contract contract;

    @Column(name = "EXPENSE_ADDITION_ID", nullable = false)
    private Addition addition;

    public Addition getAddition() {
        return addition;
    }

    public void setAddition(Addition addition) {
        this.addition = addition;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expense expense = (Expense) o;
        return Objects.equals(contract, expense.contract) &&
                Objects.equals(addition, expense.addition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contract, addition);
    }

    @Override
    public String toString() {
        return "Expense{" +
                "contract=" + contract +
                ", addition=" + addition +
                '}';
    }
}
