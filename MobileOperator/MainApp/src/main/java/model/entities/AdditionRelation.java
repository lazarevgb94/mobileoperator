package model.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "additionRelation")
public class AdditionRelation implements Serializable {

    public enum Relation {MANDATORY, MISMATCHED}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "MAIN_ADDITION_ID")
    private Addition mainAddition;

    @ManyToOne
    @JoinColumn(name = "RELATIVE_ADDITION_ID")
    private Addition relativeAddition;

    @Column(name = "RELATION")
    @Enumerated(EnumType.STRING)
    private Relation relation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Addition getMainAddition() {
        return mainAddition;
    }

    public void setMainAddition(Addition mainAddition) {
        this.mainAddition = mainAddition;
    }

    public Addition getRelativeAddition() {
        return relativeAddition;
    }

    public void setRelativeAddition(Addition relativeAddition) {
        this.relativeAddition = relativeAddition;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }
}
