package model.repositories;

import model.entities.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements CRUDRepository<T> {
    protected SessionFactory sessionFactory;
    private Class<T> classEntity;

    public AbstractRepository(Class<T> classEntity) {
        this.classEntity = classEntity;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Class<T> getClassEntity() {
        return classEntity;
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM " + classEntity.getSimpleName()).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> getAllValid() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("SELECT e FROM " + classEntity.getSimpleName()
                + " e where e.valid = 1" ).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> getAllByProperty(String property, Object value) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "SELECT e FROM " + classEntity.getSimpleName()
                        + " e where e." + property + " = :val");
        query.setParameter("val", value);
        return query.list();
    }

    public void create(T entity) {
        Session session = sessionFactory.getCurrentSession();
        session.clear();
        session.save(entity);
    }

    public int deleteByProperty(String property, Object value) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "DELETE FROM " + classEntity.getSimpleName()
                        + " where " + property + " = :val");
        query.setParameter("val", value);
        return query.executeUpdate();

    }

    public void update(T obj) {
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        session.clear();
        session.saveOrUpdate(obj);
    }

    @SuppressWarnings("unchecked")
    public T getByProperty(String property, Object value) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "FROM " + classEntity.getSimpleName()
                        + " e where e." + property + " = :val");
        query.setParameter("val", value);
        List<T> result = query.list();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public List<T> getAllValidByProperty(String property, Object value){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "SELECT e FROM " + classEntity.getSimpleName()
                        + " e where e." + property + " = :val and e.valid = 1");
        query.setParameter("val", value);
        return query.list();
    }
}
