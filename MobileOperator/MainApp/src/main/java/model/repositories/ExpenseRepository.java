package model.repositories;

import model.entities.Expense;

public interface ExpenseRepository extends CRUDRepository<Expense> {
}
