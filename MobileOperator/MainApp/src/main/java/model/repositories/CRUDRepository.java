package model.repositories;

import model.entities.AbstractEntity;
import java.util.List;

public interface CRUDRepository<T extends AbstractEntity> {
    Class<T> getClassEntity();

    List<T> getAll();

    List<T> getAllValid();

    void create(T obj);

    int deleteByProperty(String property, Object value);

    void update(T obj);

    T getByProperty(String property, Object value);

    List<T> getAllByProperty(String property, Object value);

    List<T> getAllValidByProperty(String property, Object value);
}
