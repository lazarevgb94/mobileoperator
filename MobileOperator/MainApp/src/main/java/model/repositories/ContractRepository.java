package model.repositories;

import model.entities.Contract;
import model.entities.Package;
import model.entities.Tariff;

public interface ContractRepository extends CRUDRepository<Contract> {
}
