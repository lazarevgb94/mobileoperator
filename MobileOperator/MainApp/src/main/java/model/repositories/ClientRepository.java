package model.repositories;

import model.entities.Client;
import model.entities.Contract;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ClientRepository extends CRUDRepository<Client> {
}
