package model.repositories.implementations;

import model.entities.Expense;
import model.repositories.AbstractRepository;
import model.repositories.ExpenseRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ExpenseRepositoryImpl extends AbstractRepository<Expense> implements ExpenseRepository {
    public ExpenseRepositoryImpl() {
        super(Expense.class);
    }
}
