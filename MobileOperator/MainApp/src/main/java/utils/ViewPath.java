package utils;

public final class ViewPath {
    private ViewPath() {
    }

    public static final String ACCOUNT_FORM = "accountForm";
    public static final String ACCOUNT_LIST = "accountList";
    public static final String ADDITION_FORM = "additionForm";
    public static final String ADDITION_LIST = "additionsList";
    public static final String ADMIN_PAGE = "adminPage";
    public static final String BALANCE_FORM = "balanceForm";
    public static final String CHANGE_NUMBER = "changeNumber";
    public static final String CHANGE_TARIFF = "changeTariff";
    public static final String CLIENT_ADD = "clientAdd";
    public static final String CLIENT_PAGE = "clientPage";
    public static final String CLIENT_LIST = "listClients";
    public static final String CLIENT_LOGIN_FORM = "clientLogInForm";
    public static final String CLIENT_NEW_PSW = "clientNewPsw";
    public static final String CLIENT_ENTER_PSW = "clientEnterPsw";
    public static final String CLIENT_OPTIONS = "clientOptions";
    public static final String CONTRACT_LIST = "listContracts";
    public static final String EDIT_OPTIONS = "editOptions";
    public static final String EMPLOYEE_BALANCE_FORM = "employeeBalanceForm";
    public static final String EMPLOYEE_EDIT_OPTIONS = "redirect:/employee/%d/editOptions";
    public static final String ERROR = "error";
    public static final String ERROR_LOGIN_CLIENT = "errorLoginClient";
    public static final String ERROR_NO_CONTRACT_FOUND = "errorNoContractFound";
    public static final String ERROR403 = "403";
    public static final String FIND_CLIENT_FORM = "findClientForm";
    public static final String LOGIN = "login";
    public static final String LOG_AS_EMPLOYEE = "logAsEmployee";
    public static final String LOGIN_ERROR_PAGE = "loginErrorPage";
    public static final String MANAGER_PAGE = "managerPage";
    public static final String NEW_CONTRACT = "newContract";
    public static final String PERSONAL_CONTRACT = "personalContract";
    public static final String PERSONAL_TARIFF_LIST = "personalTariffList";
    public static final String REGISTRATION = "registration";
    public static final String START_PAGE = "/returnToStartPage";
    public static final String SHOW_ADDITIONS = "showAdditions";
    public static final String SHOW_TARIFFS = "showTariffs";
    public static final String SUCCESS = "success";
    public static final String SUCCESS_ACCOUNT = "successAccount";
    public static final String TARIFF_FORM = "tariffForm";
    public static final String TARIFF_LIST = "tariffList";
    public static final String WELCOME = "welcome";

}
