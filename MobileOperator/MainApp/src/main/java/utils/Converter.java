package utils;

import model.dto.*;
import model.entities.Addition;
import model.entities.Client;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface Converter {
    /**
     * Converts instance of BigClientDto to ClientDto without tariff, option and contract number info
     *
     * @param bcd BigClientDto that has client, tariff, option and contract number info
     * @return instance of ClientDto
     */
    ClientDto convertToClientDto(BigClientDto bcd);

    /**
     * Converts instance of BigClientDto to ContractDto without client info
     *
     * @param bcd       BigClientDto that has client, tariff, option and contract number info
     * @param clientDto ClientDto has client info
     * @return instance of ContractDto
     */
    ContractDto convertToContractDto(BigClientDto bcd, ClientDto clientDto);

    /**
     * Check which options are available for this contract
     *
     * @param contractDto specified contract
     * @param additions actual list of all additions in DB
     * @return available additions
     */
    List<AdditionDto> checkAdditions(ContractDto contractDto, List<Addition> additions);

    List<AdditionDto> checkOwnAdditions(ContractDto contractDto, List<AdditionDto> ownAdditions);

    TariffDto convertToTariffDto(TariffFormDto tariffForm);

    TariffFormDto convertToTariffFormDto(TariffDto tariff);

    AdditionDto convertToAdditionDto(AdditionFormDto additionForm);

    AdditionFormDto convertToAdditionFormDto(AdditionDto addition);

    ContractDto convertToContractDtoFromForm(ContractFormDto contractFormDto, Client client);

    AdditionDto convertToAdditionDtoFromAddition(Addition addition);

    Addition convertToAdditionFromAdditionDto(AdditionDto additionDto);

    TariffDto getTariffDtoById(Integer id);

    List<AdditionDto> getListAdditionDtoById(List<Integer> additionIds);
}
