package utils;

public class ControllerPath {
    private ControllerPath() {
    }

    public static final String ADMIN = "/admin";
    public static final String ADMIN_ADD_ACCOUNT = "/admin/addAccount";
    public static final String ADMIN_SHOW_ACCOUNTS = "/admin/showAccounts";
    public static final String ADMIN_ADD_ACCOUNT_FORM = "/admin/addAccountForm";
    public static final String ADMIN_RETURN_TO_ADMIN_PAGE = "/admin/returnToAdminPage";
    public static final String CLIENT = "/client";
    public static final String CLIENT_ADD_OPTION = "/client/addClientOptions";
    public static final String CLIENT_BALANCE = "/client/balance";
    public static final String CLIENT_BLOCK_SELF = "/client/blockSelf";
    public static final String CLIENT_CHOOSE_TARIFF = "client/{tariffId}/chooseTariff";
    public static final String CLIENT_EDIT_OPTIONS = "/client/editClientOptions";
    public static final String CLIENT_LOGIN = "clientLogIn";
    public static final String CLIENT_LOGIN_FORM = "/clientLogInForm";
    public static final String CLIENT_PAGE = "/clientPage";
    public static final String CLIENT_REMOVE_OPTION = "/client/{addId}/removeClientOption";
    public static final String CLIENT_REMOVE_BASKET_TARIFF_FROM_OPTIONS = "/client/removeBasketTariffFromOptions";
    public static final String CLIENT_REMOVE_BASKET_TARIFF_FROM_TARIFFS = "/client/removeBasketTariffFromTariffs";
    public static final String CLIENT_REMOVE_BASKET_ADD_FROM_OPTIONS = "/client/{aId}/removeBasketAddFromOptions";
    public static final String CLIENT_REMOVE_BASKET_ADD_FROM_TARIFFS = "/client/{aId}/removeBasketAddFromTariffs";
    public static final String CLIENT_RETURN_TO_CLIENT_PAGE = "/client/returnToClientPage";
    public static final String CLIENT_SHOW_PERSONAL_CONTRACT = "/client/showPersonalContract";
    public static final String CLIENT_SHOW_TARIFFS = "/client/showTariffs";
    public static final String CLIENT_SUBMIT_BASKET = "/client/submitBasket";
    public static final String CREATE_ACCOUNT_FORM = "/createAccountForm";
    public static final String DIRECT = "/direct";
    public static final String EMPLOYEE = "/employee";
    public static final String EMPLOYEE_ADD_CLIENT = "/employee/addClient";
    public static final String EMPLOYEE_ADD_NUMBER = "/employee/{id}/addNumber";
    public static final String EMPLOYEE_ADDITIONS = "/employee/additions";
    public static final String EMPLOYEE_ADDITIONS_ID = "/employee/additions/{id}";
    public static final String EMPLOYEE_ADDITIONS_ADD = "/employee/additions/add";
    public static final String EMPLOYEE_ADDITIONS_UPDATE = "/employee/additions/{id}/update";
    public static final String EMPLOYEE_ADDITIONS_DELETE = "/employee/additions/{id}/delete";
    public static final String EMPLOYEE_CLIENT_BALANCE = "/employee/{id}/balance";
    public static final String EMPLOYEE_CHANGE_NUMBER = "/employee/{id}/changeNumber";
    public static final String EMPLOYEE_CHANGE_TARIFF = "/employee/{id}/changeTariff";
    public static final String EMPLOYEE_CONTRACT_BLOCK = "/employee/{contractId}/block";
    public static final String EMPLOYEE_CONTRACT_REMOVE_OPTION = "/employee/{contractId}/{addId}/removeOption";
    public static final String EMPLOYEE_DELETE_CLIENT = "/employee/{id}/deleteClient";
    public static final String EMPLOYEE_DELETE_CONTRACT = "employee/{id}/deleteContract";
    public static final String EMPLOYEE_EDIT_OPTIONS = "/employee/{id}/editOptions";
    public static final String EMPLOYEE_FIND_CLIENT = "/employee/findClient";
    public static final String EMPLOYEE_LOGIN = "/employee/logIn";
    public static final String EMPLOYEE_NEW_CLIENT = "/employee/newClient";
    public static final String EMPLOYEE_RETURN_TO_CONTRACTS = "/employee/returnToListContracts";
    public static final String EMPLOYEE_SHOW_CLIENTS = "/employee/showAllClients";
    public static final String EMPLOYEE_SHOW_CONTRACTS = "/employee/showAllContracts";
    public static final String EMPLOYEE_SHOW_SEARCH_RESULT = "/employee/showSearchResult";
    public static final String EMPLOYEE_TARIFFS = "/employee/tariffs";
    public static final String EMPLOYEE_TARIFFS_ID = "/employee/tariffs/{id}";
    public static final String EMPLOYEE_TARIFFS_ADD = "/employee/tariffs/add";
    public static final String EMPLOYEE_TARIFFS_UPDATE = "/employee/tariffs/{id}/update";
    public static final String EMPLOYEE_TARIFFS_DELETE = "/employee/tariffs/{id}/delete";
    public static final String ERROR403 = "/403";
    public static final String ID_PSW_ADD = "/{id}/pswAdd";
    public static final String ID_CONFIRM_PSW = "/confirmPsw";
    public static final String LOGIN = "/login";
    public static final String LOG_IN_FORM = "/logInForm";
    public static final String REDIRECT = "redirect:";
    public static final String REGISTRATION = "/registration";
    public static final String ROOT = "/";
    public static final String TARIFFS = "/tariffs";
    public static final String WELCOME = "/welcome";
}
