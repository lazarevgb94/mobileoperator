package utils;

public final class AttributeName {
    private AttributeName(){}
    public static final String ACCOUNT = "account";
    public static final String ACCOUNT_LIST = "accounts";
    public static final String ADDITION = "addition";
    public static final String ADDITION_LIST = "additionList";
    public static final String ADMIN = "admin";
    public static final String BASKET = "basket";
    public static final String CHOSEN_ADDITIONS = "chosenAdditions";
    public static final String CLIENT = "client";
    public static final String CLIENTS_LIST = "clientsList";
    public static final String CONTRACT = "contract";
    public static final String CONTRACT_LIST = "contractList";
    public static final String CSS = "css";
    public static final String DELETE_ADDITIONS = "deleteAdditions";
    public static final String ERROR = "error";
    public static final String EXPENSE = "expense";
    public static final String MANAGER = "manager";
    public static final String MESSAGE = "msg";
    public static final String NUMBER = "number";
    public static final String OPTIONS = "options";
    public static final String PARAMETER_ENUM = "parameterEnum";
    public static final String TARIFF = "tariff";
    public static final String TARIFF_FORM = "tariffForm";
    public static final String TARIFF_LIST = "tariffList";
    public static final String USER = "user";

}
