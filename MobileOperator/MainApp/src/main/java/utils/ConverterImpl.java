package utils;

import model.dto.*;
import model.entities.*;
import model.entities.AdditionRelation.Relation;
import model.entities.Package;
import model.repositories.AdditionRepository;
import model.repositories.TariffRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ConverterImpl implements Converter {
    private ModelMapper modelMapper;
    private TariffRepository tariffRepository;
    private AdditionRepository additionRepository;

    @Autowired
    public ConverterImpl(ModelMapper modelMapper, TariffRepository tariffRepository,
                         AdditionRepository additionRepository) {
        this.modelMapper = modelMapper;
        this.additionRepository = additionRepository;
        this.tariffRepository = tariffRepository;
    }

    private String convertDate(String date) {
        //0123456789
        //yyyy-mm-dd
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8);
        return day + "." + month + "." + year;
    }

    @Override
    public ClientDto convertToClientDto(BigClientDto bcd) {
        ClientDto client = new ClientDto();
        client.setName(bcd.getName());
        client.setSurname(bcd.getSurname());
        client.setBirthday(convertDate(bcd.getBirthday()));
        client.setPassportSeries(bcd.getPassportSeries());
        client.setPassportNumber(bcd.getPassportNumber());
        client.setPassportIssuedBy(bcd.getPassportIssuedBy());
        client.setPassportIssueDate(convertDate(bcd.getPassportIssueDate()));
        client.setPassportDivisionCode(bcd.getPassportDivisionCode());
        client.setAddress(bcd.getAddress());
        client.setEmail(bcd.getEmail());
        client.setValid(true);

        return client;
    }

    @Override
    public ContractDto convertToContractDto(BigClientDto bcd, ClientDto clientDto) {
        ContractDto contract = new ContractDto();
        contract.setNumber(bcd.getNumber());
        TariffDto tariff = getTariffDtoById(bcd.getTariffId());
        List<AdditionDto> additions = getListAdditionDtoById(bcd.getAdditionIds());
        contract.setTariff(tariff);
        contract.setAdditions(checkOwnAdditions(contract, additions));
        contract.setClient(clientDto);
        contract.setBlock(Contract.BLOCK.UNBLOCKED);
        contract.setValid(true);
        return contract;
    }

    @Override
    @Transactional(readOnly = true)
    public TariffDto getTariffDtoById(Integer id) {
        Tariff tariff = tariffRepository.getByProperty("id", id);
        return tariff == null ? null : modelMapper.map(tariff, TariffDto.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdditionDto> getListAdditionDtoById(List<Integer> additionIds) {
        List<AdditionDto> additions = new ArrayList<>();
        for (Integer id : additionIds) {
            additions.add(modelMapper.map(
                    additionRepository.getByProperty("id", id), AdditionDto.class));
        }
        return additions;
    }

    private Addition compareAdditionValues(Addition addition, List<AdditionDto> contractAdditions,
                                           Addition.Parameter parameter) {
        int value = addition.getValue();// parameter of checking option
        //iterate over all this contract's additions       (1)
        for (AdditionDto cAd : contractAdditions) {
            //if parameter of checking option is different
            if (parameter != cAd.getParameter()
                    && (contractAdditions.indexOf(cAd) == contractAdditions.size() - 1)
                    || value > cAd.getValue()) {
                //if parameters are different but it's the end of contract's
                // addition list then add current checking addition
                // or parameters are the same and the value of current checking options is greater than
                // value of option in list then add it
                return addition;
            }
        }
        return null;
    }

    private Addition checkCurrentAddition(Addition addition, List<AdditionDto> contractAdditions,
                                          TariffDto tariffDto) {
        AdditionDto adDto = modelMapper.map(addition, AdditionDto.class);
        //Check whether contract has already this addition and is it incompatible for the tariff
        if (!contractAdditions.contains(adDto) &&
                tariffDto.getPackages().stream().noneMatch(p -> p.getAddition().equals(adDto)))
            /*!tariffDto.getIncompatibleAdditions().contains(adDto))*/ {
            //get the parameter of current addition in DB
            Addition.Parameter parameter = addition.getParameter();
            //if this parameter in contract's tariff is 0 then iterate next addition
            if (parameter == Addition.Parameter.SMS && tariffDto.getSms() != 0 ||
                    parameter == Addition.Parameter.MINUTES && tariffDto.getMinutes() != 0 ||
                    parameter == Addition.Parameter.INTERNET && tariffDto.getInternet() != 0) {
                //if there are no additions on contract then add current
                if (contractAdditions.isEmpty()) {
                    return addition;
                } else {
                    return compareAdditionValues(addition, contractAdditions, parameter);
                }
            }
        }
        return null;
    }

    @Override
    public List<AdditionDto> checkAdditions(ContractDto contractDto, List<Addition> additions) {
        TariffDto tariffDto = contractDto.getTariff();
        if (tariffDto == null)
            return new ArrayList<>();
        //current chosen options of the contract
        List<AdditionDto> contractAdditions = contractDto.getAdditions();//current contract's additions
        List<Addition> availableAdditions = new ArrayList<>();//result additions
        //iterate over all additions in DB
        for (Addition ad : additions) {
            availableAdditions.add(checkCurrentAddition(ad, contractAdditions, tariffDto));
        }
        return availableAdditions.isEmpty() ? null :
                availableAdditions.stream()
                        .filter(Objects::nonNull)
                        .map(addition -> modelMapper.map(addition, AdditionDto.class))
                        .collect(Collectors.toList());
    }

    @Override
    public List<AdditionDto> checkOwnAdditions(ContractDto contractDto, List<AdditionDto> ownAdditions) {
        TariffDto tariffDto = contractDto.getTariff();
        List<AdditionDto> availableAdditions = new ArrayList<>();
        for (AdditionDto ad : ownAdditions) {
            /*if (!tariffDto.getIncompatibleAdditions().contains(ad))*/
            if (tariffDto.getPackages().stream().
                    noneMatch(packageDto -> packageDto.getAddition().equals(ad))) {
                Addition.Parameter parameter = ad.getParameter();
                if (!(parameter == Addition.Parameter.SMS && tariffDto.getSms() == 0 ||
                        parameter == Addition.Parameter.MINUTES && tariffDto.getMinutes() == 0 ||
                        parameter == Addition.Parameter.INTERNET && tariffDto.getInternet() == 0)) {
                    availableAdditions.add(ad);
                }
            }
        }
        return availableAdditions;
    }

    @Override
    public TariffDto convertToTariffDto(TariffFormDto tariffForm) {
        TariffDto tariff = new TariffDto();

        tariff.setId(tariffForm.getId());
        tariff.setName(tariffForm.getName());
        tariff.setPrice(tariffForm.getPrice());
        tariff.setDescription(tariffForm.getDescription());
        tariff.setSms(tariffForm.getSms());
        tariff.setMinutes(tariffForm.getMinutes());
        tariff.setInternet(tariffForm.getInternet());

        List<AdditionDto> additions = getListAdditionDtoById(tariffForm.getAddIds());
        List<PackageDto> packages = new ArrayList<>();
        for (AdditionDto ad : additions) {
            PackageDto packageDto = new PackageDto();
            packageDto.setTariff(tariff);
            packageDto.setAddition(ad);
            packages.add(packageDto);
        }
        tariff.setPackages(packages);

        return tariff;
    }

    @Override
    public TariffFormDto convertToTariffFormDto(TariffDto tariff) {
        TariffFormDto tariffForm = new TariffFormDto();

        tariffForm.setId(tariff.getId());
        tariffForm.setName(tariff.getName());
        tariffForm.setPrice(tariff.getPrice());
        tariffForm.setDescription(tariff.getDescription());
        tariffForm.setSms(tariff.getSms());
        tariffForm.setMinutes(tariff.getMinutes());
        tariffForm.setInternet(tariff.getInternet());

        /*List<Integer> addIds = tariff.getIncompatibleAdditions().stream()
                .map(AdditionDto::getId).collect(Collectors.toList());*/
        List<Integer> addIds = tariff.getPackages().stream()
                .map(PackageDto::getAddition)
                .map(AdditionDto::getId)
                .collect(Collectors.toList());
        tariffForm.setAddIds(addIds);

        return tariffForm;
    }

    @Override
    public AdditionDto convertToAdditionDto(AdditionFormDto additionForm) {
        AdditionDto additionDto = new AdditionDto();

        additionDto.setId(additionForm.getId());
        additionDto.setName(additionForm.getName());
        additionDto.setParameter(additionForm.getParameter());
        additionDto.setValue(additionForm.getValue());
        additionDto.setPrice(additionForm.getPrice());
        additionDto.setAdditionActivationCost(additionForm.getAdditionActivationCost());
        additionDto.setValid(true);

        for (Integer id : additionForm.getTariffIds()) {
            PackageDto packageDto = new PackageDto();
            packageDto.setTariff(getTariffDtoById(id));
            packageDto.setAddition(additionDto);
            additionDto.getPackages().add(packageDto);
        }

        additionDto.getMandatoryAdditions().addAll(
                getListAdditionDtoById(additionForm.getMandatoryIds()));
        additionDto.getIncompatibleAdditions().addAll(
                getListAdditionDtoById(additionForm.getIncompatibleIds()));

        return additionDto;
    }

    @Override
    public AdditionFormDto convertToAdditionFormDto(AdditionDto addition) {
        AdditionFormDto afd = new AdditionFormDto();

        afd.setId(addition.getId());
        afd.setName(addition.getName());
        afd.setParameter(addition.getParameter());
        afd.setValue(addition.getValue());
        afd.setPrice(addition.getPrice());
        afd.setAdditionActivationCost(addition.getAdditionActivationCost());

        return afd;
    }

    @Override
    public ContractDto convertToContractDtoFromForm(ContractFormDto contractFormDto, Client client) {
        ContractDto contractDto = new ContractDto();

        contractDto.setId(contractFormDto.getId());
        contractDto.setBlock(Contract.BLOCK.UNBLOCKED);
        contractDto.setClient(modelMapper.map(client, ClientDto.class));
        contractDto.setNumber(contractFormDto.getNumber());
        contractDto.setTariff(getTariffDtoById(contractFormDto.getTariffId()));
        contractDto.setAdditions(getListAdditionDtoById(contractFormDto.getAdditionIds()));
        contractDto.setAdditions(checkOwnAdditions(contractDto, contractDto.getAdditions()));
        contractDto.setBalance(contractFormDto.getBalance());
        contractDto.setValid(true);

        return contractDto;
    }

    @Override
    public AdditionDto convertToAdditionDtoFromAddition(Addition addition) {
        AdditionDto additionDto = new AdditionDto();

        additionDto.setId(addition.getId());
        additionDto.setName(addition.getName());
        additionDto.setParameter(addition.getParameter());
        additionDto.setValue(addition.getValue());
        additionDto.setPrice(addition.getPrice());
        additionDto.setAdditionActivationCost(addition.getAdditionActivationCost());

        List<PackageDto> packageDtos = addition.getPackages().stream()
                .map(aPackage ->  modelMapper.map(aPackage, PackageDto.class))
                .collect(Collectors.toList());
        additionDto.setPackages(packageDtos);

        Set<AdditionDto> mandatoryAdditions = new HashSet<>();
        Set<AdditionDto> incompatibleAdditions = new HashSet<>();
        addition.getMainAdditions()
                .forEach(
                        additionRelation -> {
                            if (additionRelation.getRelation().equals(Relation.MANDATORY)) {
                                mandatoryAdditions.add(modelMapper.map(
                                        additionRelation.getRelativeAddition(), AdditionDto.class));
                            } else if (additionRelation.getRelation().equals(Relation.MISMATCHED)) {
                                incompatibleAdditions.add(modelMapper.map(
                                        additionRelation.getRelativeAddition(), AdditionDto.class));
                            }
                        }
                );
        additionDto.setMandatoryAdditions(mandatoryAdditions);
        additionDto.setIncompatibleAdditions(incompatibleAdditions);
        return additionDto;
    }

    @Override
    public Addition convertToAdditionFromAdditionDto(AdditionDto additionDto) {
        Addition addition = new Addition();
        if (additionDto.getId() != null) {
            addition.setId(additionDto.getId());
        }
        addition.setName(additionDto.getName());
        addition.setParameter(additionDto.getParameter());
        addition.setValue(additionDto.getValue());
        addition.setPrice(additionDto.getPrice());
        addition.setAdditionActivationCost(additionDto.getAdditionActivationCost());

        Set<Package> packages = additionDto.getPackages().stream()
                .map(packageDto -> modelMapper.map(packageDto, Package.class)).collect(Collectors.toSet());
        addition.setPackages(packages);

        for (AdditionDto ad : additionDto.getMandatoryAdditions()) {
            AdditionRelationDto adr = new AdditionRelationDto();
            adr.setMainAddition(additionDto);
            adr.setRelativeAddition(ad);
            adr.setRelation(Relation.MANDATORY);
            /*addition.getMainAdditions().add(modelMapper.map(adr, AdditionRelation.class));*/
            addition.getMainAdditions().add(convertFormAdRelDtoToAdRel(adr));
        }

        for (AdditionDto ad : additionDto.getIncompatibleAdditions()) {
            AdditionRelationDto adr = new AdditionRelationDto();
            adr.setMainAddition(additionDto);
            adr.setRelativeAddition(ad);
            adr.setRelation(Relation.MISMATCHED);
            /*addition.getMainAdditions().add(modelMapper.map(adr, AdditionRelation.class));*/
            addition.getMainAdditions().add(convertFormAdRelDtoToAdRel(adr));
        }
        return addition;
    }

    private AdditionRelation convertFormAdRelDtoToAdRel(AdditionRelationDto adrDto){
        AdditionRelation adr = new AdditionRelation();

        adr.setMainAddition(modelMapper.map(adrDto.getMainAddition(), Addition.class));
        adr.setRelativeAddition(modelMapper.map(adrDto.getRelativeAddition(), Addition.class));
        adr.setRelation(adrDto.getRelation());

        return adr;
    }
}
