package utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Component
public class JmsProducer {
    private RabbitTemplate rabbitTemplate;
    private Queue queue;


    @Autowired
    public JmsProducer(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    public void send() {

       /* String message = "UPDATE";
        rabbitTemplate.convertAndSend(queue.getName(), message);
        System.out.println(queue.getName() + "# "  + message);*/
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare("myQueue",
                    true, false, false, null);
            channel.exchangeDeclare("myQueueExchange", "topic", true);
            channel.queueBind("myQueue", "myQueueExchange", "key");
            String message = "RabbitMQ!";
            channel.basicPublish("myQueueExchange", "key", null,
                    message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        } catch (TimeoutException | IOException ex) {
            System.out.println(ex.getMessage() + "#: " + ex.getCause());
        }
    }
}