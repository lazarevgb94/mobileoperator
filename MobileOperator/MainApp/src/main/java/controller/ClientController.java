package controller;

import model.dto.*;
import model.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import utils.ControllerPath;
import utils.ViewPath;
import utils.AttributeName;
import validators.ClientValidator;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ClientController {
    private ClientService clientService;
    private ClientValidator clientValidator;

    @Autowired
    public ClientController(ClientService clientService, ClientValidator clientValidator) {
        this.clientService = clientService;
        this.clientValidator = clientValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(clientValidator);
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_NEW_CLIENT)
    public String showForm(ModelMap model) {
        populateModel(model);
        return ViewPath.CLIENT_ADD;
    }

    private void populateModel(ModelMap model) {
        List<TariffDto> tariffList = clientService.getTariffList();
        List<AdditionDto> additionList = clientService.getAdditionList();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);
        model.addAttribute(AttributeName.ADDITION_LIST, additionList);
        model.addAttribute(AttributeName.CLIENT, new BigClientDto());
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADD_CLIENT)
    public String submit(@Valid @ModelAttribute("client") BigClientDto bigClient,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            populateModel(model);
            return ViewPath.CLIENT_ADD;
        }

        model.addAttribute("name", bigClient.getName());
        model.addAttribute("surname", bigClient.getSurname());
        model.addAttribute("birthday", bigClient.getBirthday());
        model.addAttribute("series", bigClient.getPassportSeries());
        model.addAttribute("number", bigClient.getPassportNumber());
        model.addAttribute("issuedBy", bigClient.getPassportIssuedBy());
        model.addAttribute("issueDate", bigClient.getPassportIssueDate());
        model.addAttribute("divisionCode", bigClient.getPassportDivisionCode());
        model.addAttribute("address", bigClient.getAddress());
        model.addAttribute("email", bigClient.getEmail());
        model.addAttribute("contractNumber", bigClient.getNumber());

        clientService.createFromBigClient(bigClient);
        model.addAttribute(AttributeName.CLIENTS_LIST, clientService.getAll());
        return ViewPath.CLIENT_LIST;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_SHOW_CLIENTS)
    public String showAllClients(Model model) {
        model.addAttribute(AttributeName.CLIENTS_LIST, clientService.getAll());
        return ViewPath.CLIENT_LIST;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_DELETE_CLIENT)
    public String deleteClient(@PathVariable("id") int id, Model model) {
        clientService.deleteById(id);
        model.addAttribute(AttributeName.CLIENTS_LIST, clientService.getAll());
        return ViewPath.CLIENT_LIST;
    }
}
