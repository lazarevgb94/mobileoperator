package controller;

import model.dto.AccountDto;
import model.dto.AdminDto;
import model.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import utils.AttributeName;
import utils.ViewPath;
import utils.ControllerPath;

import javax.validation.Valid;

@Controller
public class MainController {
    private AccountService accountService;

    @Autowired
    public MainController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(value = ControllerPath.LOG_IN_FORM)
    public ModelAndView logForm() {
        return new ModelAndView(ViewPath.LOG_AS_EMPLOYEE, AttributeName.ACCOUNT, new AccountDto());
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_LOGIN)
    public String logIn(@Valid @ModelAttribute("admin") final AdminDto dto) {
        if (dto.getLogin().equals("admin") && dto.getPassword().equals("admin"))
            return ViewPath.ADMIN_PAGE;
        if (dto.getLogin().equals("manager") && dto.getPassword().equals("manager"))
            return ViewPath.MANAGER_PAGE;
        else
            return ViewPath.LOGIN_ERROR_PAGE;
    }

    @GetMapping(value = ControllerPath.ADMIN_SHOW_ACCOUNTS)
    public String showAccounts(Model model) {
        model.addAttribute(AttributeName.ACCOUNT_LIST, accountService.getAll());
        return ViewPath.ACCOUNT_LIST;
    }

    @GetMapping(value = ControllerPath.ADMIN_ADD_ACCOUNT_FORM)
    public ModelAndView showAccForm() {
        return new ModelAndView(ViewPath.ACCOUNT_FORM, AttributeName.ACCOUNT, new AccountDto());
    }

    @PostMapping(value = ControllerPath.ADMIN_ADD_ACCOUNT)
    public String addAccount(@Valid @ModelAttribute("account") final AccountDto account,
                             final BindingResult result, final ModelMap model) {
        if (result.hasErrors())
            return ViewPath.ERROR;
        model.addAttribute("login", account.getLogin());
        model.addAttribute("password", account.getPassword());
        accountService.create(account);
        return ViewPath.SUCCESS_ACCOUNT;
    }

    @GetMapping(value = ControllerPath.ADMIN_RETURN_TO_ADMIN_PAGE)
    public String returnToAdminPage() {
        return ViewPath.ADMIN_PAGE;
    }
}
