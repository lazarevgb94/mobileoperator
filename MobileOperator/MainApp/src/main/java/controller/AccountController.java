package controller;

import model.dto.*;
import model.services.AccountService;
import model.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import utils.AttributeName;
import utils.ControllerPath;
import utils.ViewPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashSet;

@Controller
public class AccountController {
    private AccountService accountService;
    private HttpServletRequest request;
    private Basket basket;
    private SecurityService securityService;

    @Autowired
    public AccountController(AccountService accountService, HttpServletRequest request,
                             Basket basket, SecurityService securityService) {
        this.accountService = accountService;
        this.request = request;
        this.basket = basket;
        this.securityService = securityService;
    }

    @GetMapping(value = ControllerPath.CREATE_ACCOUNT_FORM)
    public String registration(Model model) {
        model.addAttribute(AttributeName.USER, new UserDto());

        return ViewPath.REGISTRATION;
    }

    @PostMapping(value = ControllerPath.REGISTRATION)
    public String registration(@ModelAttribute("user") @Validated UserDto user, BindingResult bindingResult) {
        if(!user.getPassword().equals(user.getConfirmPassword())){
            return ViewPath.REGISTRATION;
        }

        if (bindingResult.hasErrors()) {
            return ViewPath.REGISTRATION;
        }
        ContractDto contractDto = accountService.getContractByNumber(user.getNumber());
        if (contractDto == null) {
            return ViewPath.ERROR_NO_CONTRACT_FOUND;
        }
        accountService.update(user);
        securityService.autoLogin(user.getNumber(), user.getConfirmPassword());

        return ControllerPath.REDIRECT + ControllerPath.WELCOME;
    }

    @GetMapping(value = {ControllerPath.ROOT, ControllerPath.WELCOME})
    public String welcome() {
        return ViewPath.WELCOME;
    }

    @GetMapping(value = ControllerPath.DIRECT)
    public String direct() {
        String number = request.getUserPrincipal().getName();
        switch (number) {
            case "admin":
                return ControllerPath.REDIRECT + ControllerPath.ADMIN;
            case "manager":
                return ControllerPath.REDIRECT + ControllerPath.EMPLOYEE;
            default:
                return ControllerPath.REDIRECT + ControllerPath.CLIENT;
        }
    }

    @GetMapping(value = ControllerPath.CLIENT)
    public String client(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();

        //initial settings for session
        HttpSession session = request.getSession();
        if (!session.isNew()) {
            session.invalidate();
            session = request.getSession(true);
        }
        session.setAttribute(AttributeName.TARIFF, new TariffDto());
        session.setAttribute(AttributeName.CHOSEN_ADDITIONS, new HashSet<AdditionDto>());
        session.setAttribute(AttributeName.DELETE_ADDITIONS, new HashSet<AdditionDto>());

        basket.clear();
        ContractDto contract = accountService.getContractByNumber(number);
        model.addAttribute(AttributeName.CONTRACT, contract);

        return ViewPath.CLIENT_PAGE;
    }

    @GetMapping(value = ControllerPath.LOGIN)
    public String loginForm(Model model, String error) {
        model.addAttribute(AttributeName.ERROR, "Error is here!");
        model.addAttribute(AttributeName.CONTRACT, new ContractDto());
        return ViewPath.LOGIN;
    }

    @PostMapping(value = ControllerPath.LOGIN)
    public String login(@ModelAttribute("contract") ContractDto contractDto) {
        ContractDto contract = accountService.getContractByNumber(contractDto.getNumber());
        return ControllerPath.REDIRECT +  ControllerPath.CLIENT_PAGE
                + ControllerPath.ROOT + contract.getId();
    }

    @GetMapping(value = ControllerPath.EMPLOYEE)
    public String managerPage() {
        return ViewPath.MANAGER_PAGE;
    }

    // for 403 access denied page
    @GetMapping(value = ControllerPath.ERROR403)
    public ModelAndView accessDenied(Principal user) {
        ModelAndView model = new ModelAndView();
        ContractDto contractDto = accountService.getContractByNumber(user.getName());

        model.addObject(AttributeName.MESSAGE, "Unfortunately, " +
                contractDto.getClient().getName() + ", you are forbidden " +
                "to access the manager's page :'( ");
        model.setViewName(ViewPath.ERROR403);
        return model;

    }
}
