package controller;

import model.dto.AdditionDto;
import model.dto.AdditionFormDto;
import model.dto.TariffDto;
import model.entities.Addition;
import model.services.AdditionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import utils.AttributeName;
import utils.ControllerPath;
import utils.Converter;
import utils.ViewPath;

import java.util.Arrays;
import java.util.List;

@Controller
public class AdditionController {
    private AdditionService additionService;
    private Converter converter;

    @Autowired
    public AdditionController(AdditionService additionService, Converter converter) {
        this.additionService = additionService;
        this.converter = converter;
    }

    private final Logger logger = LoggerFactory.getLogger(AdditionController.class);

    @GetMapping(value = ControllerPath.EMPLOYEE_ADDITIONS)
    public String showAllAdditions(ModelMap model) {
        logger.debug("showAllAdditions()");
        model.addAttribute(AttributeName.ADDITION_LIST, additionService.getAll());
        return ViewPath.ADDITION_LIST;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADDITIONS)
    public String saveOrUpdateAddition(@ModelAttribute("addition")/* @Validated*/ AdditionFormDto additionForm,
                                       BindingResult result, Model model,
                                       final RedirectAttributes redirectAttributes) {
        logger.debug("saveOrUpdateAddition() : {}", additionForm);
        if (result.hasErrors()) {
            model.addAttribute(AttributeName.ADDITION, additionForm);
            return ViewPath.ADDITION_FORM;
        } else {
            redirectAttributes.addFlashAttribute(AttributeName.CSS, "success");
            if (additionForm.isNew()) {
                redirectAttributes.addFlashAttribute(
                        AttributeName.MESSAGE, "Addition added successfully!");
            } else {
                redirectAttributes.addFlashAttribute(
                        AttributeName.MESSAGE, "Addition successfully updated!");
            }
            additionService.saveOrUpdate(additionForm);
            return ControllerPath.REDIRECT + ControllerPath.EMPLOYEE_ADDITIONS;
        }
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_ADDITIONS_ADD)
    public String showAddOptionForm(Model model) {

        logger.debug("showAddOptionForm()");
        model.addAttribute(AttributeName.ADDITION, new AdditionFormDto());
        List<Addition.Parameter> parameters = Arrays.asList(Addition.Parameter.class.getEnumConstants());
        model.addAttribute(AttributeName.PARAMETER_ENUM, parameters);
        List<TariffDto> tariffList = additionService.getAllTariffs();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);
        List<AdditionDto> additionList = additionService.getAll();
        model.addAttribute(AttributeName.ADDITION_LIST, additionList);

        return ViewPath.ADDITION_FORM;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADDITIONS_UPDATE)
    public String showUpdateOptionForm(@PathVariable("id") int id, Model model) {

        logger.debug("showUpdateOptionForm() : {}", id);

        AdditionDto addition = additionService.getById(id);
        AdditionFormDto additionFormDto = converter.convertToAdditionFormDto(addition);
        model.addAttribute(AttributeName.ADDITION, additionFormDto);
        List<Addition.Parameter> parameters = Arrays.asList(Addition.Parameter.class.getEnumConstants());
        model.addAttribute(AttributeName.PARAMETER_ENUM, parameters);
        List<TariffDto> tariffList = additionService.getAllTariffs();
        model.addAttribute("tariffList", tariffList);

        return ViewPath.ADDITION_FORM;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADDITIONS_DELETE)
    public String deleteAddition(@PathVariable("id") int id,
                                 final RedirectAttributes redirectAttributes) {

        logger.debug("deleteAddition() : {}", id);

        additionService.deleteById(id);

        redirectAttributes.addFlashAttribute(
                AttributeName.CSS, "success");
        redirectAttributes.addFlashAttribute(
                AttributeName.MESSAGE, "Addition is deleted!");

        return ControllerPath.REDIRECT + ControllerPath.EMPLOYEE_ADDITIONS;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADDITIONS_ID)
    public String showAddition(@PathVariable("id") int id, Model model) {

        logger.debug("showAddition() id: {}", id);

        AdditionDto addition = additionService.getById(id);
        if (addition == null) {
            model.addAttribute(AttributeName.CSS, "danger");
            model.addAttribute(AttributeName.MESSAGE, "Addition not found");
        }
        model.addAttribute(AttributeName.ADDITION, addition);

        return ViewPath.SHOW_ADDITIONS;
    }
}
