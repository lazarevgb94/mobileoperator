package controller;

import model.dto.*;
import model.services.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import utils.AttributeName;
import utils.ControllerPath;
import utils.ViewPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class ContractController {
    private ContractService contractService;
    private HttpServletRequest request;
    private Basket basket;

    @Autowired
    public ContractController(ContractService contractService, HttpServletRequest request,
                              Basket basket) {
        this.contractService = contractService;
        this.request = request;
        this.basket = basket;
    }

    @PostMapping(value = ControllerPath.ID_PSW_ADD)
    public String pswAdd(@PathVariable("id") int id,
                         @ModelAttribute("contract") final ContractDto dto) {
        ContractDto contractDto = contractService.getById(id);
        contractDto.setPassword(dto.getPassword());
        contractService.updateContract(contractDto);
        return ViewPath.SUCCESS;
    }

    @PostMapping(value = ControllerPath.ID_CONFIRM_PSW)
    public String confirmPsw(@ModelAttribute("contract") final ContractDto dto) {
        ContractDto contractDto = contractService.getByPassword(dto.getPassword());
        if (contractDto == null)
            return ViewPath.ERROR_LOGIN_CLIENT;
        else {
            return "redirect:" + ControllerPath.CLIENT;
        }
    }

    @SuppressWarnings("unchecked")
    private void populateBasket(HttpSession session, String attribute) {
        switch (attribute) {
            case AttributeName.TARIFF:
                TariffDto tDto = ((TariffDto) session.getAttribute(AttributeName.TARIFF));
                basket.chooseTariff(tDto);
                break;
            case AttributeName.CHOSEN_ADDITIONS:
                Set<AdditionDto> adds = (Set<AdditionDto>) session.getAttribute(AttributeName.CHOSEN_ADDITIONS);
                basket.addAddition(adds);
                session.setAttribute(AttributeName.CHOSEN_ADDITIONS, new HashSet<AdditionDto>());
                break;
            case AttributeName.DELETE_ADDITIONS:
                basket.getDeletedAdditions().addAll(
                        (Set<AdditionDto>) session.getAttribute(AttributeName.DELETE_ADDITIONS));
                basket.removeAllAdditions(basket.getDeletedAdditions());
                session.setAttribute(AttributeName.DELETE_ADDITIONS, new HashSet<AdditionDto>());
                break;
            default:
                break;
        }
    }

    @GetMapping(value = ControllerPath.CLIENT_LOGIN_FORM)
    public ModelAndView clientLogInForm() {
        return new ModelAndView(ViewPath.CLIENT_LOGIN_FORM, AttributeName.CONTRACT, new ContractDto());
    }

    @PostMapping(value = ControllerPath.CLIENT_LOGIN)
    public String clientLogIn(@ModelAttribute("contract") final ContractDto dto,
                              final ModelMap model) {
        ContractDto contract = contractService.getByNumber(dto.getNumber());
        if (contract == null)
            return ViewPath.ERROR_LOGIN_CLIENT;
        String password = contract.getPassword();
        model.addAttribute(AttributeName.CONTRACT, contract);
        if (password == null) {
            return ViewPath.CLIENT_NEW_PSW;
        } else {
            return ViewPath.CLIENT_ENTER_PSW;
        }
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_SHOW_CONTRACTS)
    public String showAllContracts(Model model) {
        model.addAttribute(AttributeName.CONTRACT_LIST, contractService.getAll());
        return ViewPath.CONTRACT_LIST;
    }

    @GetMapping(value = ControllerPath.CLIENT_SHOW_PERSONAL_CONTRACT)
    public String showPersonalContract(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);
        model.addAttribute(AttributeName.CONTRACT, contract);
        return ViewPath.PERSONAL_CONTRACT;
    }

    @GetMapping(value = ControllerPath.CLIENT_SHOW_TARIFFS)
    public String showTariffs(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();

        List<TariffDto> tariffList = contractService.getTariffList();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);
        ContractDto contract = contractService.getByNumber(number);
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.BASKET, basket);
        return ViewPath.PERSONAL_TARIFF_LIST;
    }

    @PostMapping(value = ControllerPath.CLIENT_CHOOSE_TARIFF)
    public String chooseTariff(@PathVariable("tariffId") int tId, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);

        TariffDto tariff = contractService.getTariffById(tId);
        model.addAttribute(AttributeName.CONTRACT, contract);
        HttpSession session = request.getSession();
        session.setAttribute(AttributeName.TARIFF, tariff);
        populateBasket(session, AttributeName.TARIFF);

        List<TariffDto> tariffList = contractService.getTariffList();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);
        model.addAttribute(AttributeName.BASKET, basket);
        return ViewPath.PERSONAL_TARIFF_LIST;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_CONTRACT_BLOCK)
    public String blockContract(@PathVariable("contractId") int id) {
        ContractDto contract = contractService.getById(id);
        contractService.inverseBlockStatus(contract);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CONTRACTS;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_CHANGE_NUMBER)
    public ModelAndView showChangeNumber(@PathVariable("id") int id) {
        return new ModelAndView(ViewPath.CHANGE_NUMBER, AttributeName.CONTRACT, contractService.getById(id));
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_CHANGE_NUMBER)
    public String changeNumber(@PathVariable("id") int id,
                               @ModelAttribute("contract") ContractDto dto) {
        ContractDto contract = contractService.getById(id);
        contract.setNumber(dto.getNumber());
        contractService.updateContract(contract);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CONTRACTS;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_CHANGE_TARIFF)
    public String showChangeTariff(@PathVariable("id") int id, Model model) {
        model.addAttribute(AttributeName.CONTRACT, contractService.getById(id));
        model.addAttribute(AttributeName.TARIFF_LIST, contractService.getTariffList());
        return ViewPath.CHANGE_TARIFF;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_CHANGE_TARIFF)
    public String changeTariff(@PathVariable("id") int id,
                               @ModelAttribute("contract") ContractDto dto) {
        ContractDto contract = contractService.getById(id);
        int tariffId = dto.getTariff().getId();
        contract.setTariff(contractService.getTariffById(tariffId));
        contractService.updateContract(contract);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CONTRACTS;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_EDIT_OPTIONS)
    public String showEditOptions(@PathVariable("id") int id, Model model) {
        ContractDto contract = contractService.getById(id);
        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.EXPENSE, expense);

        return ViewPath.EDIT_OPTIONS;
    }

    @GetMapping(value = ControllerPath.CLIENT_EDIT_OPTIONS)
    public String showEditClientOptions(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.EXPENSE, expense);

        model.addAttribute(AttributeName.BASKET, basket);

        return ViewPath.CLIENT_OPTIONS;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_CONTRACT_REMOVE_OPTION)
    public String removeOption(@PathVariable("contractId") int cId,
                               @PathVariable("addId") int aId, Model model) {
        ContractDto contract = contractService.getById(cId);
        contract.getAdditions().remove(contractService.getAdditionById(aId));
        contractService.updateContract(contract);
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.OPTIONS, contractService.getAvailableOptions(contract));

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.EXPENSE, expense);

        return String.format(ViewPath.EMPLOYEE_EDIT_OPTIONS, cId);
    }

    @SuppressWarnings("unchecked")
    @PostMapping(value = ControllerPath.CLIENT_REMOVE_OPTION)
    public String removeClientOption(@PathVariable("addId") int aId, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);
        AdditionDto additionDto = contractService.getAdditionById(aId);

        HttpSession session = request.getSession();
        Set<AdditionDto> delAdds = (Set<AdditionDto>) session.getAttribute(AttributeName.DELETE_ADDITIONS);
        delAdds.add(additionDto);
        session.setAttribute(AttributeName.DELETE_ADDITIONS, delAdds);
        populateBasket(session, AttributeName.DELETE_ADDITIONS);
        model.addAttribute(AttributeName.BASKET, basket);

        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.OPTIONS, contractService.getAvailableOptions(contract));

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.EXPENSE, expense);

        return ControllerPath.REDIRECT + ControllerPath.CLIENT_EDIT_OPTIONS;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_EDIT_OPTIONS)
    public String editOptions(@PathVariable("id") int id,
                              @ModelAttribute("contract") ContractDto dto,
                              @ModelAttribute("expense") ExpenseDto expenseDto,
                              Model model) {
        if (expenseDto.getOptionIds().isEmpty()) return ViewPath.EDIT_OPTIONS;
        ContractDto contract = contractService.getById(id);
        contract.getAdditions().addAll(contractService.getAdditionListByIdList(expenseDto.getOptionIds()));
        contractService.updateContract(contract);

        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.OPTIONS, contractService.getAvailableOptions(contract));

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.EXPENSE, expense);
        return ViewPath.EDIT_OPTIONS;
    }

    @SuppressWarnings("unchecked")
    @PostMapping(value = ControllerPath.CLIENT_ADD_OPTION)
    public String editClientOptions(@ModelAttribute("contract") ContractDto dto,
                                    @ModelAttribute("expense") ExpenseDto expenseDto,
                                    Model model) {
        if (expenseDto.getOptionIds().isEmpty()) return ViewPath.EDIT_OPTIONS;
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);

        HttpSession session = request.getSession();
        Set<AdditionDto> chAdds = (Set<AdditionDto>) session.getAttribute(AttributeName.CHOSEN_ADDITIONS);
        chAdds.addAll(contractService.getAdditionListByIdList(expenseDto.getOptionIds()));
        session.setAttribute(AttributeName.CHOSEN_ADDITIONS, chAdds);
        populateBasket(session, AttributeName.CHOSEN_ADDITIONS);
        model.addAttribute(AttributeName.BASKET, basket);

        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.OPTIONS, contractService.getAvailableOptions(contract));

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.EXPENSE, expense);
        return ViewPath.CLIENT_OPTIONS;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_RETURN_TO_CONTRACTS)
    public ModelAndView returnToListContracts() {
        return new ModelAndView(ViewPath.CONTRACT_LIST, AttributeName.CONTRACT_LIST, contractService.getAll());
    }

    @GetMapping(value = ControllerPath.CLIENT_RETURN_TO_CLIENT_PAGE)
    public String clientPage() {
        return "redirect:" + ControllerPath.CLIENT;
    }

    @PostMapping(value = ControllerPath.CLIENT_SUBMIT_BASKET)
    public String submitBasket() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        contractService.submitBasket(contractService.getByNumber(number), basket);

        HttpSession session = request.getSession();
        session.invalidate();
        session = request.getSession(true);
        session.setAttribute(AttributeName.TARIFF, new TariffDto());
        session.setAttribute(AttributeName.CHOSEN_ADDITIONS, new HashSet<AdditionDto>());
        session.setAttribute(AttributeName.DELETE_ADDITIONS, new HashSet<AdditionDto>());

        return "redirect:" + ControllerPath.CLIENT;
    }

    @PostMapping(value = ControllerPath.CLIENT_REMOVE_BASKET_TARIFF_FROM_OPTIONS)
    public String removeBasketTariffFromOptions(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);
        basket.removeTariff();

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.EXPENSE, expense);

        model.addAttribute(AttributeName.BASKET, basket);
        return ViewPath.CLIENT_OPTIONS;
    }

    @PostMapping(value = ControllerPath.CLIENT_REMOVE_BASKET_TARIFF_FROM_TARIFFS)
    public String removeBasketTariffFromTariff(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);
        basket.removeTariff();
        model.addAttribute(AttributeName.BASKET, basket);

        model.addAttribute(AttributeName.CONTRACT, contract);
        List<TariffDto> tariffList = contractService.getTariffList();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);

        return ViewPath.PERSONAL_TARIFF_LIST;
    }

    @PostMapping(value = ControllerPath.CLIENT_REMOVE_BASKET_ADD_FROM_OPTIONS)
    public String removeBasketAddFromOptions(@PathVariable("aId") int aId, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);

        AdditionDto add = contractService.getAdditionById(aId);
        basket.removeBasketAdd(add);

        ExpenseDto expense = new ExpenseDto();
        expense.setContract(contract);
        expense.setOptionIds(contractService.getAvailableOptionsIds(contract));
        expense.setOptions(contractService.getAvailableOptions(contract));
        model.addAttribute(AttributeName.CONTRACT, contract);
        model.addAttribute(AttributeName.EXPENSE, expense);

        model.addAttribute(AttributeName.BASKET, basket);
        return ViewPath.CLIENT_OPTIONS;
    }

    @PostMapping(value = ControllerPath.CLIENT_REMOVE_BASKET_ADD_FROM_TARIFFS)
    public String removeBasketAddFromTariffs(@PathVariable("aId") int aId, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);

        AdditionDto add = contractService.getAdditionById(aId);
        basket.removeBasketAdd(add);

        model.addAttribute(AttributeName.CONTRACT, contract);
        List<TariffDto> tariffList = contractService.getTariffList();
        model.addAttribute(AttributeName.TARIFF_LIST, tariffList);

        model.addAttribute(AttributeName.BASKET, basket);
        return ViewPath.PERSONAL_TARIFF_LIST;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_FIND_CLIENT)
    public String showFindForm(ModelMap modelMap) {
        modelMap.addAttribute(AttributeName.CONTRACT, new ContractDto());
        modelMap.addAttribute(AttributeName.CONTRACT_LIST, contractService.getAll());
        return ViewPath.FIND_CLIENT_FORM;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_SHOW_SEARCH_RESULT)
    public String showSearchResult(@ModelAttribute("contract") final ContractDto contract,
                                   final ModelMap model) {
        ClientDto clientDto = contractService.getById(contract.getId()).getClient();
        model.addAttribute(AttributeName.CLIENT, clientDto);
        model.addAttribute(AttributeName.CONTRACT, new ContractDto());
        model.addAttribute(AttributeName.CONTRACT_LIST, contractService.getAll());
        return ViewPath.FIND_CLIENT_FORM;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_ADD_NUMBER)
    public String addNumberForm(@PathVariable("id") int id, Model model) {
        model.addAttribute(AttributeName.CLIENT, contractService.getClientById(id));
        model.addAttribute(AttributeName.CONTRACT, new ContractFormDto());
        model.addAttribute(AttributeName.TARIFF_LIST, contractService.getTariffList());
        model.addAttribute(AttributeName.ADDITION_LIST, contractService.getAllAdditions());
        return ViewPath.NEW_CONTRACT;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_ADD_NUMBER)
    public String addNumber(@PathVariable("id") int id,
                            @ModelAttribute("contract") ContractFormDto contractFormDto) {
        contractService.addClientContract(id, contractFormDto);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CLIENTS;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_DELETE_CONTRACT)
    public String deleteContract(@PathVariable("id") int id) {
        contractService.deleteById(id);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CONTRACTS;
    }

    @GetMapping(value = ControllerPath.CLIENT_BALANCE)
    public String getBalance(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        ContractDto contract = contractService.getByNumber(number);
        model.addAttribute(AttributeName.CONTRACT, contract);
        return ViewPath.BALANCE_FORM;
    }

    @PostMapping(value = ControllerPath.CLIENT_BALANCE)
    public String setBalance(@ModelAttribute("contract") ContractDto contractDto) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ContractDto contract = contractService.getByNumber(user.getUsername());
        contract.setBalance(contract.getBalance().add(contractDto.getBalance()));
        contractService.updateContract(contract);
        return "redirect:" + ControllerPath.CLIENT;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_CLIENT_BALANCE)
    public String getClientBalance(@PathVariable("id") int id, Model model) {
        ContractDto contractDto = contractService.getById(id);
        model.addAttribute(AttributeName.CONTRACT, contractDto);
        return ViewPath.EMPLOYEE_BALANCE_FORM;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_CLIENT_BALANCE)
    public String setClientBalance(@PathVariable("id") int id,
                                   @ModelAttribute("contract") ContractDto dto) {
        ContractDto contract = contractService.getById(id);
        contract.setBalance(contract.getBalance().add(dto.getBalance()));
        contractService.updateContract(contract);
        return "redirect:" + ControllerPath.EMPLOYEE_SHOW_CONTRACTS;
    }
}
