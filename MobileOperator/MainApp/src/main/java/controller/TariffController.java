package controller;

import model.dto.AdditionDto;
import model.dto.TariffDto;
import model.dto.TariffFormDto;
import model.services.TariffService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import utils.*;

import java.util.List;

@Controller
public class TariffController {
    private TariffService tariffService;
    private Converter converter;
    private JmsProducer producer;

    @Autowired
    public TariffController(TariffService tariffService, Converter converter,
                            JmsProducer producer) {
        this.tariffService = tariffService;
        this.converter = converter;
        this.producer = producer;
    }

    private final Logger logger = LoggerFactory.getLogger(TariffController.class);

    @GetMapping(value = ControllerPath.EMPLOYEE_TARIFFS)
    public String showAllTariffs(Model model) {
        logger.debug("showAllTariffs()");
        model.addAttribute(AttributeName.TARIFF_LIST, tariffService.getAll());
        producer.send();
        return ViewPath.TARIFF_LIST;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_TARIFFS)
    public String saveOrUpdateTariff(@ModelAttribute("tariff") /*@Validated */TariffFormDto tariffForm,
                                     BindingResult result, final RedirectAttributes redirectAttributes) {
        logger.debug("saveOrUpdateTariff() : {}", tariffForm);
        if (result.hasErrors()) {
            return ViewPath.TARIFF_FORM;
        } else {
            redirectAttributes.addFlashAttribute(AttributeName.CSS, "success");
            if (tariffForm.isNew()) {
                redirectAttributes.addFlashAttribute(
                        AttributeName.MESSAGE, "Tariff added successfully!");
            } else {
                redirectAttributes.addFlashAttribute(
                        AttributeName.MESSAGE, "Tariff successfully updated!");
            }

            tariffService.saveOrUpdate(tariffForm);
            /*producer.send();*/

            return ControllerPath.REDIRECT + ControllerPath.EMPLOYEE_TARIFFS;
        }
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_TARIFFS_ADD)
    public String showAddTariffForm(Model model) {
        logger.debug("showAddTariffForm()");
        List<AdditionDto> additions = tariffService.getAllAdditions();
        model.addAttribute(AttributeName.TARIFF, new TariffFormDto());
        model.addAttribute(AttributeName.ADDITION_LIST, additions);


        return ViewPath.TARIFF_FORM;
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_TARIFFS_UPDATE)
    public String showUpdateTariffForm(@PathVariable("id") int id, Model model) {
        logger.debug("showUpdateTariffForm() : {}", id);

        TariffDto tariff = tariffService.getById(id);
        TariffFormDto tariffFormDto = converter.convertToTariffFormDto(tariff);
        model.addAttribute(AttributeName.TARIFF, tariffFormDto);
        List<AdditionDto> additions = tariffService.getAllAdditions();
        model.addAttribute(AttributeName.ADDITION_LIST, additions);

        return "tariffForm";
    }

    @PostMapping(value = ControllerPath.EMPLOYEE_TARIFFS_DELETE)
    public String deleteTariff(@PathVariable("id") int id,
                               final RedirectAttributes redirectAttributes) {
        logger.debug("deleteTariff() : {}", id);

        tariffService.deleteById(id);
        redirectAttributes.addFlashAttribute(
                AttributeName.CSS, "success");
        redirectAttributes.addFlashAttribute(
                AttributeName.MESSAGE, "Tariff is deleted!");

        return ControllerPath.REDIRECT + ControllerPath.EMPLOYEE_TARIFFS;
    }

    @GetMapping(value = ControllerPath.EMPLOYEE_TARIFFS_ID)
    public String showTariff(@PathVariable("id") int id, Model model) {
        logger.debug("showTariff() id: {}", id);

        TariffDto tariff = tariffService.getById(id);
        if (tariff == null) {
            model.addAttribute(AttributeName.CSS, "danger");
            model.addAttribute(AttributeName.MESSAGE, "Tariff not found");
        }
        model.addAttribute(AttributeName.TARIFF, tariff);

        return ViewPath.SHOW_TARIFFS;
    }
}
