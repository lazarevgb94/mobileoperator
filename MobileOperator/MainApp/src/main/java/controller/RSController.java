package controller;

import model.dto.Basket;
import model.dto.ContractDto;
import model.dto.TariffDto;
import model.services.ContractService;
import model.services.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import utils.ControllerPath;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class RSController {
    private ContractService contractService;
    private HttpServletRequest request;
    private Basket basket;
    private TariffService tariffService;

    @Autowired
    public RSController(ContractService contractService, HttpServletRequest request,
                        Basket basket, TariffService tariffService){
        this.contractService = contractService;
        this.request = request;
        this.basket = basket;
        this.tariffService = tariffService;
    }

    @GetMapping(value = ControllerPath.CLIENT_SHOW_PERSONAL_CONTRACT,
    produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ContractDto showPersContract() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();
        return contractService.getByNumber(number);
    }

    @PostMapping(value = ControllerPath.CLIENT_BLOCK_SELF,
    produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ContractDto blockSelf() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String number = user.getUsername();

        ContractDto contract = contractService.getByNumber(number);
        contractService.inverseClientBlockStatus(contract);
        return contractService.getByNumber(number);
    }

    @GetMapping(value = ControllerPath.TARIFFS)
    public List<TariffDto> restGetTariffs(){
        return tariffService.getAll();
    }

}
