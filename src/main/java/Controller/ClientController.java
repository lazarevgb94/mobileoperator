package Controller;

import model.Client;
import model.ClientService;
import model.Contract;
import model.Tariff;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class ClientController {

    private ClientService clientService = new ClientService();

    @RequestMapping(value = "/{clientName}/" + ControllerPath.GET_CONTRACT,
            method = RequestMethod.GET)
    public Contract getCurrentContract(@PathVariable Client client) {
        return clientService.getContract(client);
    }

    @RequestMapping(value = "/{clientName}/" + ControllerPath.GET_AVAILABLE_TARRIFS,
            method = RequestMethod.GET)
    public Set<Tariff> getAvailableTariffs(@PathVariable Client client){
        return clientService.getAvailableTariffs(client);
    }
}
