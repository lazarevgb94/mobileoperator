package model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * DTO for Tariff.
 * It contains: tariff's name, its price and set of available options.
 */
@Getter
@Setter
public class TariffDto {
    private String name;
    private double price;
    private Set<OptionDto> availableOptions;
}
