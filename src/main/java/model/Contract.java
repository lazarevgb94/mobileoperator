package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Presents table Contract in DB
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "Contract")
public class Contract {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private long id;

    @Column(name = "number")
    private String number;

    @Column(name = "tariff")                            //TODO
    private long tariffId;

    @Column(name="activeOptions")                       //TODO
    private List<Option> activeOptions;
}
