package model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * DTO for Passport
 * It contains: series and passport number, who and when issued this passport
 * and the code of division issued it.
 */
@Getter
@Setter
public class PassportDto {
    private String series;
    private String number;
    private String issuedBy;
    private String divisionCode;
    private Date issueDate;
}
