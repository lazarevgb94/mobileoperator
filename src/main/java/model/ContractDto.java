package model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * DTO for Contract
 * It contains: contract (telephone) number, chosen tariff
 * and list of active options for this tariff.
 */
@Getter
@Setter
public class ContractDto {
    private String number;
    private long tariffId;
    private List<Option> activeOptions;
}
