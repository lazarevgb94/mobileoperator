package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Presents table Passport in DB
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Embeddable
@Table(name = "Passport")
public class Passport {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private long id;

    @Column(name = "series")
    private String series;

    @Column(name = "number")
    private String number;

    @Column(name = "issuedBy")
    private String issuedBy;

    @Column(name = "divisionCode")
    private String divisionCode;

    @Column(name = "issueDate")
    private Date issueDate;
}
