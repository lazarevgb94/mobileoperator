package model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * DTO for Client.
 * It contains: client's name and surname, client's birthday, passport data,
 * the address, number of contract, email and password for client's personal account.
 */
@Getter
@Setter
public class ClientDto {
    private String name;
    private String surname;
    private Date birthday;
    private Passport passport;
    private String address;
    private String contractNumber;
    private String email;
    private String password;
}
