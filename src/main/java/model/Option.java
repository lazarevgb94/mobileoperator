package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Presents table Option in DB
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "Option")
public class Option {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Column(name = "optionActivationCost")
    private double optionActivationCost;
}
