package model;

import utils.DBConnection;
import utils.SQLQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientService {
    private Connection connection;

    public ClientService() {
        this.connection = DBConnection.getConnection();
    }

    public Contract getContract(Client client) {
        Contract contract = new Contract();
        try {
            PreparedStatement ps = connection.prepareStatement(SQLQueries.GET_CONTRACT +
                    client.getId());
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                contract.setId(result.getInt("id"));
                contract.setNumber(result.getString("number"));
                contract.setTariffId(result.getInt("tariff_id"));
                /*
                Array options = result.getArray("optionIds");
                if(options instanceof ArrayList)
                    contract.setActiveOptions((List<Option>) options));*/
                contract.setActiveOptions(
                        (List<Option>) result.getArray("optionIds"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return contract;
    }

    public Set<Tariff> getAvailableTariffs(Client client) {
        Set<Tariff> tariffs = new HashSet<>();
        try {
            PreparedStatement ps = connection.prepareStatement(
                    SQLQueries.GET_AVAILABLE_TARRIFS);
            ResultSet result = ps.executeQuery();
            while (result.next()){
                Tariff tariff = new Tariff.TariffBuilder().
                        id(result.getInt("id")).
                        name(result.getString("name")).
                        price(result.getDouble("price")).
                        build();
                tariffs.add(tariff);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return tariffs;
    }
}
