package model;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO for Option
 * It contains: option's name, its price and a cost of option's service activation
 */
@Getter
@Setter
public class OptionDto {
    private String name;
    private double price;
    private double optionActivationCost;
}
