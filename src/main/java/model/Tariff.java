package model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Presents table Tariff in DB
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "Tariff")
public class Tariff {
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name="price")
    private double price;

    @Column(name="availableOptions")
    private Set<OptionDto> availableOptions;
}
