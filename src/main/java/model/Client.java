package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Presents table Client in DB
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "Client")
public class Client {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name="birthday")
    private Date birthday;

    @Embedded
    @Column(name="passport")                //TODO
    private Passport passport;

    @Column(name="address")
    private String address;

    @Column(name = "contractNumber")
    private String contractNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;
}
