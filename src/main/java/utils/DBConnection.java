package utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null)
            return connection;
        else {
            try {
                Properties prop = new Properties();
                InputStream is = DBConnection.class.getClassLoader().
                        getResourceAsStream("resources/config.properties");
                prop.load(is);
                String driver = prop.getProperty("driver");
                String url = prop.getProperty("url");
                String user = prop.getProperty("user");
                String password = prop.getProperty("password");
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                System.out.println(e.getCause().getMessage());
            } catch (ClassNotFoundException e) {
                System.out.println(e.getCause().getMessage());
            } catch (IOException e) {
                System.out.println(e.getCause().getMessage());
            }
            return connection;
        }
    }
}